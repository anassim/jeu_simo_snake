/****************************************************************************
** Meta object code from reading C++ file 'snakemultjoueurs.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../simo-snake/snakemultjoueurs.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'snakemultjoueurs.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Connexion_t {
    QByteArrayData data[8];
    char stringdata0[89];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Connexion_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Connexion_t qt_meta_stringdata_Connexion = {
    {
QT_MOC_LITERAL(0, 0, 9), // "Connexion"
QT_MOC_LITERAL(1, 10, 11), // "seConnecter"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 13), // "lancerServeur"
QT_MOC_LITERAL(4, 37, 13), // "seDeconnecter"
QT_MOC_LITERAL(5, 51, 9), // "Connecter"
QT_MOC_LITERAL(6, 61, 15), // "lancerUnServeur"
QT_MOC_LITERAL(7, 77, 11) // "Deconnecter"

    },
    "Connexion\0seConnecter\0\0lancerServeur\0"
    "seDeconnecter\0Connecter\0lancerUnServeur\0"
    "Deconnecter"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Connexion[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,
       3,    0,   45,    2, 0x06 /* Public */,
       4,    0,   46,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   47,    2, 0x09 /* Protected */,
       6,    0,   48,    2, 0x09 /* Protected */,
       7,    0,   49,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Connexion::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Connexion *_t = static_cast<Connexion *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->seConnecter(); break;
        case 1: _t->lancerServeur(); break;
        case 2: _t->seDeconnecter(); break;
        case 3: _t->Connecter(); break;
        case 4: _t->lancerUnServeur(); break;
        case 5: _t->Deconnecter(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (Connexion::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Connexion::seConnecter)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Connexion::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Connexion::lancerServeur)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (Connexion::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Connexion::seDeconnecter)) {
                *result = 2;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject Connexion::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Connexion.data,
      qt_meta_data_Connexion,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Connexion::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Connexion::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Connexion.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int Connexion::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void Connexion::seConnecter()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Connexion::lancerServeur()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void Connexion::seDeconnecter()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
struct qt_meta_stringdata_SnakeMultijoueurs_t {
    QByteArrayData data[26];
    char stringdata0[333];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SnakeMultijoueurs_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SnakeMultijoueurs_t qt_meta_stringdata_SnakeMultijoueurs = {
    {
QT_MOC_LITERAL(0, 0, 17), // "SnakeMultijoueurs"
QT_MOC_LITERAL(1, 18, 20), // "snakeMultijoueursOut"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 15), // "newMultijoueurs"
QT_MOC_LITERAL(4, 56, 2), // "Ip"
QT_MOC_LITERAL(5, 59, 4), // "port"
QT_MOC_LITERAL(6, 64, 5), // "timer"
QT_MOC_LITERAL(7, 70, 9), // "connecter"
QT_MOC_LITERAL(8, 80, 15), // "lancerUnServeur"
QT_MOC_LITERAL(9, 96, 11), // "deconnecter"
QT_MOC_LITERAL(10, 108, 17), // "nouvelleConnexion"
QT_MOC_LITERAL(11, 126, 13), // "donneesRecues"
QT_MOC_LITERAL(12, 140, 17), // "deconnexionClient"
QT_MOC_LITERAL(13, 158, 8), // "connecte"
QT_MOC_LITERAL(14, 167, 10), // "deconnecte"
QT_MOC_LITERAL(15, 178, 12), // "erreurSocket"
QT_MOC_LITERAL(16, 191, 28), // "QAbstractSocket::SocketError"
QT_MOC_LITERAL(17, 220, 10), // "rejouerSon"
QT_MOC_LITERAL(18, 231, 8), // "position"
QT_MOC_LITERAL(19, 240, 13), // "keyPressEvent"
QT_MOC_LITERAL(20, 254, 10), // "QKeyEvent*"
QT_MOC_LITERAL(21, 265, 5), // "event"
QT_MOC_LITERAL(22, 271, 23), // "chargerObjectifPosition"
QT_MOC_LITERAL(23, 295, 16), // "snakeTranslation"
QT_MOC_LITERAL(24, 312, 12), // "envoyerSnake"
QT_MOC_LITERAL(25, 325, 7) // "gameEnd"

    },
    "SnakeMultijoueurs\0snakeMultijoueursOut\0"
    "\0newMultijoueurs\0Ip\0port\0timer\0connecter\0"
    "lancerUnServeur\0deconnecter\0"
    "nouvelleConnexion\0donneesRecues\0"
    "deconnexionClient\0connecte\0deconnecte\0"
    "erreurSocket\0QAbstractSocket::SocketError\0"
    "rejouerSon\0position\0keyPressEvent\0"
    "QKeyEvent*\0event\0chargerObjectifPosition\0"
    "snakeTranslation\0envoyerSnake\0gameEnd"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SnakeMultijoueurs[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   99,    2, 0x06 /* Public */,
       3,    3,  100,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    0,  107,    2, 0x09 /* Protected */,
       8,    0,  108,    2, 0x09 /* Protected */,
       9,    0,  109,    2, 0x09 /* Protected */,
      10,    0,  110,    2, 0x09 /* Protected */,
      11,    0,  111,    2, 0x09 /* Protected */,
      12,    0,  112,    2, 0x09 /* Protected */,
      13,    0,  113,    2, 0x09 /* Protected */,
      14,    0,  114,    2, 0x09 /* Protected */,
      15,    1,  115,    2, 0x09 /* Protected */,
      17,    1,  118,    2, 0x09 /* Protected */,
      19,    1,  121,    2, 0x09 /* Protected */,
      22,    0,  124,    2, 0x09 /* Protected */,
      23,    0,  125,    2, 0x09 /* Protected */,
      24,    0,  126,    2, 0x09 /* Protected */,
      25,    0,  127,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::UShort, QMetaType::Int,    4,    5,    6,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 16,    2,
    QMetaType::Void, QMetaType::LongLong,   18,
    QMetaType::Void, 0x80000000 | 20,   21,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SnakeMultijoueurs::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SnakeMultijoueurs *_t = static_cast<SnakeMultijoueurs *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->snakeMultijoueursOut(); break;
        case 1: _t->newMultijoueurs((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 2: _t->connecter(); break;
        case 3: _t->lancerUnServeur(); break;
        case 4: _t->deconnecter(); break;
        case 5: _t->nouvelleConnexion(); break;
        case 6: _t->donneesRecues(); break;
        case 7: _t->deconnexionClient(); break;
        case 8: _t->connecte(); break;
        case 9: _t->deconnecte(); break;
        case 10: _t->erreurSocket((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 11: _t->rejouerSon((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 12: _t->keyPressEvent((*reinterpret_cast< QKeyEvent*(*)>(_a[1]))); break;
        case 13: _t->chargerObjectifPosition(); break;
        case 14: _t->snakeTranslation(); break;
        case 15: _t->envoyerSnake(); break;
        case 16: _t->gameEnd(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractSocket::SocketError >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (SnakeMultijoueurs::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SnakeMultijoueurs::snakeMultijoueursOut)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (SnakeMultijoueurs::*_t)(QString , quint16 , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SnakeMultijoueurs::newMultijoueurs)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject SnakeMultijoueurs::staticMetaObject = {
    { &QGraphicsView::staticMetaObject, qt_meta_stringdata_SnakeMultijoueurs.data,
      qt_meta_data_SnakeMultijoueurs,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SnakeMultijoueurs::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SnakeMultijoueurs::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SnakeMultijoueurs.stringdata0))
        return static_cast<void*>(this);
    return QGraphicsView::qt_metacast(_clname);
}

int SnakeMultijoueurs::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGraphicsView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void SnakeMultijoueurs::snakeMultijoueursOut()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void SnakeMultijoueurs::newMultijoueurs(QString _t1, quint16 _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
