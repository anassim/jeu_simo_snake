/****************************************************************************
** Meta object code from reading C++ file 'menusnake.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../simo-snake/menusnake.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'menusnake.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Map_t {
    QByteArrayData data[6];
    char stringdata0[54];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Map_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Map_t qt_meta_stringdata_Map = {
    {
QT_MOC_LITERAL(0, 0, 3), // "Map"
QT_MOC_LITERAL(1, 4, 13), // "lancerMapGame"
QT_MOC_LITERAL(2, 18, 0), // ""
QT_MOC_LITERAL(3, 19, 5), // "jouer"
QT_MOC_LITERAL(4, 25, 17), // "changerBackground"
QT_MOC_LITERAL(5, 43, 10) // "currentMap"

    },
    "Map\0lancerMapGame\0\0jouer\0changerBackground\0"
    "currentMap"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Map[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   30,    2, 0x09 /* Protected */,
       4,    1,   31,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    5,

       0        // eod
};

void Map::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Map *_t = static_cast<Map *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->lancerMapGame(); break;
        case 1: _t->jouer(); break;
        case 2: _t->changerBackground((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (Map::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Map::lancerMapGame)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject Map::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Map.data,
      qt_meta_data_Map,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Map::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Map::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Map.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int Map::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void Map::lancerMapGame()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
struct qt_meta_stringdata_SnakeMenu_t {
    QByteArrayData data[14];
    char stringdata0[188];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SnakeMenu_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SnakeMenu_t qt_meta_stringdata_SnakeMenu = {
    {
QT_MOC_LITERAL(0, 0, 9), // "SnakeMenu"
QT_MOC_LITERAL(1, 10, 20), // "menuSimpleGameClique"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 17), // "menuMapGameClique"
QT_MOC_LITERAL(4, 50, 7), // "mapName"
QT_MOC_LITERAL(5, 58, 22), // "menuMultijoueursClique"
QT_MOC_LITERAL(6, 81, 22), // "menuSnakeOptionsClique"
QT_MOC_LITERAL(7, 104, 21), // "menuSnakeScoresClique"
QT_MOC_LITERAL(8, 126, 10), // "rejouerSon"
QT_MOC_LITERAL(9, 137, 8), // "position"
QT_MOC_LITERAL(10, 146, 15), // "mousePressEvent"
QT_MOC_LITERAL(11, 162, 12), // "QMouseEvent*"
QT_MOC_LITERAL(12, 175, 5), // "event"
QT_MOC_LITERAL(13, 181, 6) // "getMap"

    },
    "SnakeMenu\0menuSimpleGameClique\0\0"
    "menuMapGameClique\0mapName\0"
    "menuMultijoueursClique\0menuSnakeOptionsClique\0"
    "menuSnakeScoresClique\0rejouerSon\0"
    "position\0mousePressEvent\0QMouseEvent*\0"
    "event\0getMap"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SnakeMenu[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x06 /* Public */,
       3,    1,   55,    2, 0x06 /* Public */,
       5,    0,   58,    2, 0x06 /* Public */,
       6,    0,   59,    2, 0x06 /* Public */,
       7,    0,   60,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    1,   61,    2, 0x09 /* Protected */,
      10,    1,   64,    2, 0x09 /* Protected */,
      13,    0,   67,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::LongLong,    9,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,

       0        // eod
};

void SnakeMenu::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SnakeMenu *_t = static_cast<SnakeMenu *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->menuSimpleGameClique(); break;
        case 1: _t->menuMapGameClique((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->menuMultijoueursClique(); break;
        case 3: _t->menuSnakeOptionsClique(); break;
        case 4: _t->menuSnakeScoresClique(); break;
        case 5: _t->rejouerSon((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 6: _t->mousePressEvent((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 7: _t->getMap(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (SnakeMenu::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SnakeMenu::menuSimpleGameClique)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (SnakeMenu::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SnakeMenu::menuMapGameClique)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (SnakeMenu::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SnakeMenu::menuMultijoueursClique)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (SnakeMenu::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SnakeMenu::menuSnakeOptionsClique)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (SnakeMenu::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SnakeMenu::menuSnakeScoresClique)) {
                *result = 4;
                return;
            }
        }
    }
}

const QMetaObject SnakeMenu::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_SnakeMenu.data,
      qt_meta_data_SnakeMenu,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SnakeMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SnakeMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SnakeMenu.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int SnakeMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void SnakeMenu::menuSimpleGameClique()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void SnakeMenu::menuMapGameClique(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void SnakeMenu::menuMultijoueursClique()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void SnakeMenu::menuSnakeOptionsClique()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void SnakeMenu::menuSnakeScoresClique()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
