/****************************************************************************
** Meta object code from reading C++ file 'snake.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../simo-snake/snake.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'snake.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Snake_t {
    QByteArrayData data[19];
    char stringdata0[285];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Snake_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Snake_t qt_meta_stringdata_Snake = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Snake"
QT_MOC_LITERAL(1, 6, 21), // "menuSimpleGameCliqued"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 15), // "SimpleGameOuted"
QT_MOC_LITERAL(4, 45, 20), // "newSimpleGameCliqued"
QT_MOC_LITERAL(5, 66, 18), // "menuMapGameCliqued"
QT_MOC_LITERAL(6, 85, 7), // "mapName"
QT_MOC_LITERAL(7, 93, 12), // "MapGameOuted"
QT_MOC_LITERAL(8, 106, 17), // "newMapGameCliqued"
QT_MOC_LITERAL(9, 124, 23), // "menuMultijoueursCliqued"
QT_MOC_LITERAL(10, 148, 17), // "MultijoueursOuted"
QT_MOC_LITERAL(11, 166, 22), // "newMultijoueursCliqued"
QT_MOC_LITERAL(12, 189, 2), // "Ip"
QT_MOC_LITERAL(13, 192, 4), // "port"
QT_MOC_LITERAL(14, 197, 5), // "timer"
QT_MOC_LITERAL(15, 203, 23), // "menuSnakeOptionsCliqued"
QT_MOC_LITERAL(16, 227, 17), // "snakeOptionsOuted"
QT_MOC_LITERAL(17, 245, 22), // "menuSnakeScoresCliqued"
QT_MOC_LITERAL(18, 268, 16) // "snakeScoresOuted"

    },
    "Snake\0menuSimpleGameCliqued\0\0"
    "SimpleGameOuted\0newSimpleGameCliqued\0"
    "menuMapGameCliqued\0mapName\0MapGameOuted\0"
    "newMapGameCliqued\0menuMultijoueursCliqued\0"
    "MultijoueursOuted\0newMultijoueursCliqued\0"
    "Ip\0port\0timer\0menuSnakeOptionsCliqued\0"
    "snakeOptionsOuted\0menuSnakeScoresCliqued\0"
    "snakeScoresOuted"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Snake[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x09 /* Protected */,
       3,    0,   80,    2, 0x09 /* Protected */,
       4,    0,   81,    2, 0x09 /* Protected */,
       5,    1,   82,    2, 0x09 /* Protected */,
       7,    0,   85,    2, 0x09 /* Protected */,
       8,    1,   86,    2, 0x09 /* Protected */,
       9,    0,   89,    2, 0x09 /* Protected */,
      10,    0,   90,    2, 0x09 /* Protected */,
      11,    3,   91,    2, 0x09 /* Protected */,
      15,    0,   98,    2, 0x09 /* Protected */,
      16,    0,   99,    2, 0x09 /* Protected */,
      17,    0,  100,    2, 0x09 /* Protected */,
      18,    0,  101,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::UShort, QMetaType::Int,   12,   13,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Snake::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Snake *_t = static_cast<Snake *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->menuSimpleGameCliqued(); break;
        case 1: _t->SimpleGameOuted(); break;
        case 2: _t->newSimpleGameCliqued(); break;
        case 3: _t->menuMapGameCliqued((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->MapGameOuted(); break;
        case 5: _t->newMapGameCliqued((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->menuMultijoueursCliqued(); break;
        case 7: _t->MultijoueursOuted(); break;
        case 8: _t->newMultijoueursCliqued((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 9: _t->menuSnakeOptionsCliqued(); break;
        case 10: _t->snakeOptionsOuted(); break;
        case 11: _t->menuSnakeScoresCliqued(); break;
        case 12: _t->snakeScoresOuted(); break;
        default: ;
        }
    }
}

const QMetaObject Snake::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Snake.data,
      qt_meta_data_Snake,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Snake::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Snake::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Snake.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int Snake::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
