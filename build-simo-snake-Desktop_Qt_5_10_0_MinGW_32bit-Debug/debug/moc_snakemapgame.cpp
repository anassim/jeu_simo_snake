/****************************************************************************
** Meta object code from reading C++ file 'snakemapgame.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../simo-snake/snakemapgame.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'snakemapgame.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SnakeMapGame_t {
    QByteArrayData data[12];
    char stringdata0[141];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SnakeMapGame_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SnakeMapGame_t qt_meta_stringdata_SnakeMapGame = {
    {
QT_MOC_LITERAL(0, 0, 12), // "SnakeMapGame"
QT_MOC_LITERAL(1, 13, 15), // "snakeMapGameOut"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 10), // "newMapGame"
QT_MOC_LITERAL(4, 41, 7), // "mapName"
QT_MOC_LITERAL(5, 49, 10), // "rejouerSon"
QT_MOC_LITERAL(6, 60, 8), // "position"
QT_MOC_LITERAL(7, 69, 13), // "keyPressEvent"
QT_MOC_LITERAL(8, 83, 10), // "QKeyEvent*"
QT_MOC_LITERAL(9, 94, 5), // "event"
QT_MOC_LITERAL(10, 100, 23), // "chargerObjectifPosition"
QT_MOC_LITERAL(11, 124, 16) // "snakeTranslation"

    },
    "SnakeMapGame\0snakeMapGameOut\0\0newMapGame\0"
    "mapName\0rejouerSon\0position\0keyPressEvent\0"
    "QKeyEvent*\0event\0chargerObjectifPosition\0"
    "snakeTranslation"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SnakeMapGame[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,
       3,    1,   45,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   48,    2, 0x09 /* Protected */,
       7,    1,   51,    2, 0x09 /* Protected */,
      10,    0,   54,    2, 0x09 /* Protected */,
      11,    0,   55,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,

 // slots: parameters
    QMetaType::Void, QMetaType::LongLong,    6,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SnakeMapGame::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SnakeMapGame *_t = static_cast<SnakeMapGame *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->snakeMapGameOut(); break;
        case 1: _t->newMapGame((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->rejouerSon((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 3: _t->keyPressEvent((*reinterpret_cast< QKeyEvent*(*)>(_a[1]))); break;
        case 4: _t->chargerObjectifPosition(); break;
        case 5: _t->snakeTranslation(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (SnakeMapGame::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SnakeMapGame::snakeMapGameOut)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (SnakeMapGame::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SnakeMapGame::newMapGame)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject SnakeMapGame::staticMetaObject = {
    { &QGraphicsView::staticMetaObject, qt_meta_stringdata_SnakeMapGame.data,
      qt_meta_data_SnakeMapGame,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SnakeMapGame::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SnakeMapGame::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SnakeMapGame.stringdata0))
        return static_cast<void*>(this);
    return QGraphicsView::qt_metacast(_clname);
}

int SnakeMapGame::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGraphicsView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void SnakeMapGame::snakeMapGameOut()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void SnakeMapGame::newMapGame(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
