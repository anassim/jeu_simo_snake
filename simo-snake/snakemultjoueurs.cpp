#include <QApplication>
#include <QObject>
#include <QMainWindow>
#include <QLabel>
#include <QEvent>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsProxyWidget>
#include <QGraphicsPixmapItem>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QCloseEvent>
#include <QTimer>
#include <QTime>
#include <QList>
#include <QMediaPlayer>
#include <cmath>
#include <sstream>
#include <QByteArray>
#include <QDataStream>
#include <QMessageBox>
#include "snakemultjoueurs.h"

enum direction{m_up,m_down,m_right,m_left};

//=============================================================================================================================
                            // LES METHODES DU Snake Multijoueurs

SnakeMultijoueurs::SnakeMultijoueurs()
{
    rejouer = false;
    /* se connecter a un serveur ou lancer un serveur*/
    m_connecter = new Connexion;
    m_client = new QTcpSocket(this);
    m_serveur = new QTcpServer(this);
    QObject::connect(m_connecter,SIGNAL(seConnecter()),this,SLOT(connecter()));
    QObject::connect(m_connecter,SIGNAL(lancerServeur()),this,SLOT(lancerUnServeur()));
    QObject::connect(m_connecter,SIGNAL(seDeconnecter()),this,SLOT(deconnecter()));
    m_connecter->exec();

    /*inisialisation des options du Multijoueurs*/
    m_scores_options = new QSettings("scores_options/scores_options.ini",QSettings::IniFormat,this);
    chargerOptions();

    /* inisialisation de la scene du simple jeu*/
    m_sonMultijoueurs = new QMediaPlayer(this);
    m_sonMultijoueurs->setMedia(QUrl("sons/son multijoueurs.mp3"));
    m_sonMultijoueurs->setVolume(60);
    m_sonMultijoueurs->play();

    m_chargerPositionSnakeTime = new QTimer(this);
    m_chargerPositionSnakeTime->setInterval(90);
    m_chargerPositionSnakeTime->start();

    m_pauseImage = new QGraphicsPixmapItem;
    m_pauseImage = m_scene->addPixmap(QPixmap("imgs/SnakeImage/Pause.png"));
    m_pauseImage->setPos(0,0);
    m_pauseImage->setZValue(4);
    m_pauseImage->hide();
    m_afficherScore = new QGraphicsTextItem;
    m_pauseOut = true;

    m_eatingObjSound = new QMediaPlayer(this);
    m_eatingObjSound->setMedia(QUrl("sons/eatingObjectif.mp3"));

    m_sonGameOver = new QMediaPlayer(this);
    m_sonGameOver->setMedia(QUrl("sons/game over.mp3"));
    m_sonGoodGame = new QMediaPlayer(this);
    m_sonGoodGame->setMedia(QUrl("sons/good game.mp3"));

    setScene(m_scene);
    setFixedSize(502,502);//fixer la taille a 502 pour ne pas avoir a resizer la scene
    setRenderHint(QPainter::Antialiasing);
    grabKeyboard();//on fait le focus des keypress sur ce widget

    QObject::connect(m_chargerPositionSnakeTime,SIGNAL(timeout()),this,SLOT(snakeTranslation()));
    QObject::connect(m_sonMultijoueurs,SIGNAL(positionChanged(qint64)),this,SLOT(rejouerSon(qint64)));
}
SnakeMultijoueurs::SnakeMultijoueurs(QString Ip,quint16 port,int timing)
{
    rejouer = false;
    /* se connecter a un serveur ou lancer un serveur*/
    m_connecter = new Connexion(Ip,port,timing);
    m_client = new QTcpSocket(this);
    m_serveur = new QTcpServer(this);
    QObject::connect(m_connecter,SIGNAL(seConnecter()),this,SLOT(connecter()));
    QObject::connect(m_connecter,SIGNAL(lancerServeur()),this,SLOT(lancerUnServeur()));
    QObject::connect(m_connecter,SIGNAL(seDeconnecter()),this,SLOT(deconnecter()));
    m_connecter->exec();

    /*inisialisation des options du Multijoueurs*/
    m_scores_options = new QSettings("scores_options/scores_options.ini",QSettings::IniFormat,this);
    chargerOptions();

    /* inisialisation de la scene du simple jeu*/
    m_sonMultijoueurs = new QMediaPlayer(this);
    m_sonMultijoueurs->setMedia(QUrl("sons/son multijoueurs.mp3"));
    m_sonMultijoueurs->setVolume(60);
    m_sonMultijoueurs->play();

    m_chargerPositionSnakeTime = new QTimer(this);
    m_chargerPositionSnakeTime->setInterval(90);
    m_chargerPositionSnakeTime->start();

    m_pauseImage = new QGraphicsPixmapItem;
    m_pauseImage = m_scene->addPixmap(QPixmap("imgs/SnakeImage/Pause.png"));
    m_pauseImage->setPos(0,0);
    m_pauseImage->setZValue(4);
    m_pauseImage->hide();
    m_afficherScore = new QGraphicsTextItem;
    m_pauseOut = true;

    m_eatingObjSound = new QMediaPlayer(this);
    m_eatingObjSound->setMedia(QUrl("sons/eatingObjectif.mp3"));

    m_sonGameOver = new QMediaPlayer(this);
    m_sonGameOver->setMedia(QUrl("sons/game over.mp3"));
    m_sonGoodGame = new QMediaPlayer(this);
    m_sonGoodGame->setMedia(QUrl("sons/good game.mp3"));

    setScene(m_scene);
    setFixedSize(502,502);//fixer la taille a 502 pour ne pas avoir a resizer la scene
    setRenderHint(QPainter::Antialiasing);
    grabKeyboard();//on fait le focus des keypress sur ce widget

    QObject::connect(m_chargerPositionSnakeTime,SIGNAL(timeout()),this,SLOT(snakeTranslation()));
    QObject::connect(m_sonMultijoueurs,SIGNAL(positionChanged(qint64)),this,SLOT(rejouerSon(qint64)));
}
void SnakeMultijoueurs::connecter()
{
    m_connecter->setEtat("essai de connexion");
    m_client->connectToHost(m_connecter->getIp(),m_connecter->getPort());
    connect(m_client, SIGNAL(readyRead()), this, SLOT(donneesRecues()));
    connect(m_client, SIGNAL(connected()), this, SLOT(connecte()));
    connect(m_client, SIGNAL(disconnected()), this, SLOT(deconnecte()));
    connect(m_client, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(erreurSocket(QAbstractSocket::SocketError)));
    //ON MAIT LE SLOT DE SOCKETERROR
}
void SnakeMultijoueurs::connecte()
{
    Ip = m_connecter->getIp();
    port = m_connecter->getPort();
    timer = m_connecter->getTime();
    m_connecter->close();
    m_connecter->deleteLater();

    m_scene = new QGraphicsScene;
    m_scene->setSceneRect(0,0,500,500);
    m_scene->addPixmap(QPixmap("imgs/Multijoueurs/background multijoueurs.png"))->setPos(QPointF(-1,-1));

    m_snakePosition.setX(40);
    m_snakePosition.setY(261);

    m_snakeAdvPosition.setX(430);
    m_snakeAdvPosition.setY(261);

    m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/rasse khdar right 3D.png")));
    m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos khdar right petit 3D.png")));
    m_snakeDirections.push_back(m_right);
    m_snakeDirections.push_back(m_right);

    m_snakeAdvImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/rasse hmar left 3D.png")));
    m_snakeAdvImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos hmar left petit 3D.png")));

    m_snakeImages.at(0)->setPos(m_snakePosition);
    m_snakeImages.at(1)->setPos(m_snakePosition.x()-20,m_snakePosition.y());

    m_snakeAdvImages.at(0)->setPos(m_snakeAdvPosition);
    m_snakeAdvImages.at(1)->setPos(m_snakeAdvPosition.x()+30,m_snakeAdvPosition.y());

    m_envoieSnakeTime = new QTimer(this);
    m_envoieSnakeTime->setInterval(150);
    m_envoieSnakeTime->start();

    m_objectif = new QGraphicsPixmapItem;//pour charger la position de l'objectif
    m_objectif = m_scene->addPixmap(QPixmap("imgs/SnakeImage/Objectif.png"));

    m_tailleMsg = 0;
    m_objectifX = 0;
    m_objectifY = 0;
    m_typeMsg = 0;

    m_pause = false;

    m_gameTime = new QTimer(this);
    m_gameTime->setInterval(timer);
    m_gameTime->start();

    m_chargerPositionObjectifTime = new QTimer(this);

    QObject::connect(m_envoieSnakeTime,SIGNAL(timeout()),this,SLOT(envoyerSnake()));
    QObject::connect(m_gameTime,SIGNAL(timeout()),this,SLOT(gameEnd()));
}
void SnakeMultijoueurs::deconnecte()
{
   emit snakeMultijoueursOut();
}
void SnakeMultijoueurs::donneesRecues()
{
    QDataStream in(m_client);

    if (m_tailleMsg == 0)
    {
        if (m_client->bytesAvailable() < static_cast<int>(sizeof(quint16)))
            return;
        in >> m_tailleMsg;
    }
    if (m_typeMsg == 0)
    {
        if(m_client->bytesAvailable() < static_cast<int>(sizeof(quint8)))
            return;
        in >> m_typeMsg;
    }
    if (m_client->bytesAvailable() < m_tailleMsg)
    {
        return;
    }

    if (m_typeMsg == 1)
    {
        QString msg,x,y;
        in >> msg;
        for (int i(0);msg[i] != "-";i++)
        {
            x += msg[i];
        }
        for (int j(msg.indexOf('-')+1);j < msg.size();j++)
        {
            y+= msg[j];
        }
        m_objectifX = x.toInt();
        m_objectifY = y.toInt();
        m_objectif->setPos(m_objectifX,m_objectifY);//on change la position de l'obj apres le calcul de la precision
        if (m_chargerPositionObjectifTime->isActive())
        {
            m_chargerPositionObjectifTime->start();
        }
    }
    else if (m_typeMsg == 2)
    {
        QString msg,x,y,direction(""),tail("");
        in >> msg;
        for (int j(0);j < m_snakeAdvImages.size();j++)
        {
            m_scene->removeItem(m_snakeAdvImages[j]);
        }
        m_snakeAdvImages.clear();

        int j(1);
        for (int i(0);i < msg.size();i++)
        {
            tail += msg[i];
            if (msg[i] == "$")
            {
               tail.remove(i,1);
                direction += tail[0];
               while (tail[j] != ",")
               {
                x += tail[j];
                j++;
               }
                j++;
               while (j < tail.size())
               {
                y += tail[j];
                j++;
               }
               if (m_snakeAdvImages.size() == 0)
               {
                   switch (direction.toInt())
                   {
                   case m_right:
                       m_snakeAdvImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/rasse hmar right 3D.png")));
                       m_snakeAdvImages[m_snakeAdvImages.size()-1]->setPos(x.toInt(),y.toInt());
                      break;
                  case m_left:
                       m_snakeAdvImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/rasse hmar left 3D.png")));
                       m_snakeAdvImages[m_snakeAdvImages.size()-1]->setPos(x.toInt(),y.toInt());
                       break;
                  case m_up:
                       m_snakeAdvImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/rasse hmar up 3D.png")));
                       m_snakeAdvImages[m_snakeAdvImages.size()-1]->setPos(x.toInt(),y.toInt());
                       break;
                  case m_down:
                       m_snakeAdvImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/rasse hmar down 3D.png")));
                       m_snakeAdvImages[m_snakeAdvImages.size()-1]->setPos(x.toInt(),y.toInt());
                       break;
                   }
               }
               else
               {
                   y.chop(1);
                   switch (direction.toInt())
                   {
                   case m_right:
                       m_snakeAdvImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos hmar right petit 3D.png")));
                       m_snakeAdvImages[m_snakeAdvImages.size()-1]->setPos(x.toInt(),y.toInt());
                      break;
                  case m_left:
                       m_snakeAdvImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos hmar left petit 3D.png")));
                       m_snakeAdvImages[m_snakeAdvImages.size()-1]->setPos(x.toInt(),y.toInt());
                       break;
                  case m_up:
                       m_snakeAdvImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos hmar up petit 3D.png")));
                       m_snakeAdvImages[m_snakeAdvImages.size()-1]->setPos(x.toInt(),y.toInt());
                       break;
                  case m_down:
                       m_snakeAdvImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos hmar down petit 3D.png")));
                       m_snakeAdvImages[m_snakeAdvImages.size()-1]->setPos(x.toInt(),y.toInt());
                       break;
                   }
               }
               j = 1;
               tail.clear();
               x.clear();
               y.clear();
               direction.clear();
            }
        }
    }
    else if (m_typeMsg == 3)
    {
        m_sonMultijoueurs->stop();
        m_chargerPositionSnakeTime->stop();
        m_chargerPositionObjectifTime->stop();

        QString msg;
        in >> msg;
        if (m_snakeImages.size() >= msg.toInt())
        {
            QString score;score.setNum(m_snakeImages.size());
            m_pauseImage->setPixmap(QPixmap("imgs/Multijoueurs/Good Game.png"));
            m_afficherScore = m_scene->addText(score);
            m_afficherScore->setPos(290,120);
            m_afficherScore->setFont(QFont("Lucida Bright",17,Qt::black));
            m_afficherScore->setZValue(5);
            /*ecrire les options*/
            imagePause = m_pauseImage->pixmap();
            m_scores_options->beginGroup("options");
            QPainter dessinOptions;
            dessinOptions.begin(&imagePause);
            dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
            dessinOptions.setFont(QFont("Impact",25));
            dessinOptions.drawText(QPoint(353,335),m_scores_options->value("rejouer").toString());
            dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
            dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
            dessinOptions.end();
            m_pauseImage->setPixmap(imagePause);
            m_scores_options->endGroup();
            m_sonGoodGame->play();
            theEndResult(2);
        }
        else if (msg.toInt() > m_snakeImages.size())
        {
            m_pauseImage->setPixmap(QPixmap("imgs/Multijoueurs/Game Over.png"));
            /*ecrire les options*/
            imagePause = m_pauseImage->pixmap();
            m_scores_options->beginGroup("options");
            QPainter dessinOptions;
            dessinOptions.begin(&imagePause);
            dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
            dessinOptions.setFont(QFont("Impact",25));
            dessinOptions.drawText(QPoint(353,335),m_scores_options->value("rejouer").toString());
            dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
            dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
            dessinOptions.end();
            m_pauseImage->setPixmap(imagePause);
            m_scores_options->endGroup();
            m_sonGameOver->play();
            theEndResult(1);
        }

        m_pauseImage->show();
    }
    else if (m_typeMsg == 4)
    {
        int result;
        in >> result;
        if (result == 1)
        {
            QString score;score.setNum(m_snakeImages.size());
            m_pauseImage->setPixmap(QPixmap("imgs/Multijoueurs/Good Game.png"));
            m_afficherScore = m_scene->addText(score);
            m_afficherScore->setPos(290,120);
            m_afficherScore->setFont(QFont("Lucida Bright",17,Qt::black));
            m_afficherScore->setZValue(5);
            /*ecrire les options*/
            imagePause = m_pauseImage->pixmap();
            m_scores_options->beginGroup("options");
            QPainter dessinOptions;
            dessinOptions.begin(&imagePause);
            dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
            dessinOptions.setFont(QFont("Impact",25));
            dessinOptions.drawText(QPoint(353,335),m_scores_options->value("rejouer").toString());
            dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
            dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
            dessinOptions.end();
            m_pauseImage->setPixmap(imagePause);
            m_scores_options->endGroup();
            m_sonGoodGame->play();
        }
        else if (result == 2)
        {
            m_pauseImage->setPixmap(QPixmap("imgs/Multijoueurs/Game Over.png"));
            /*ecrire les options*/
            imagePause = m_pauseImage->pixmap();
            m_scores_options->beginGroup("options");
            QPainter dessinOptions;
            dessinOptions.begin(&imagePause);
            dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
            dessinOptions.setFont(QFont("Impact",25));
            dessinOptions.drawText(QPoint(353,335),m_scores_options->value("rejouer").toString());
            dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
            dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
            dessinOptions.end();
            m_pauseImage->setPixmap(imagePause);
            m_scores_options->endGroup();
            m_sonGameOver->play();
        }

        m_pauseImage->show();
    }
    else if (m_typeMsg == 5)
    {
      QString msg;
      in >> msg;

      rejouer = true;
      m_client->disconnectFromHost();
    }

    m_tailleMsg = 0;
    m_typeMsg = 0;
}
void SnakeMultijoueurs::erreurSocket(QAbstractSocket::SocketError erreur)
{
    if (erreur)
    {
    }
    if (m_serveur->isListening())
    {
        m_client->disconnectFromHost();
        m_serveur->close();
    }
    else
    {
        m_client->disconnectFromHost();
    }
}

void SnakeMultijoueurs::lancerUnServeur()
{
    m_connecter->setEtat("lancement du serveur");
    if (!m_serveur->listen(QHostAddress::Any,m_connecter->getPort()))
        {
            m_connecter->setEtat("le serveur n'a pas pu etre demare.La raison :<br/>"+m_serveur->errorString());
        }
        else
        {
            m_connecter->setEtat("le serveur a ete demare sur le port <strong>"+QString::number(m_serveur->serverPort())+"</strong><br/>un client peut maintenant se connecter");
            connect(m_serveur,SIGNAL(newConnection()),this,SLOT(nouvelleConnexion()));
        }
}
void SnakeMultijoueurs::nouvelleConnexion()
{
     m_client = m_serveur->nextPendingConnection();
     Ip = m_connecter->getIp();
     port = m_connecter->getPort();
     m_connecter->close();
     m_connecter->deleteLater();

     m_scene = new QGraphicsScene;
     m_scene->setSceneRect(0,0,500,500);
     m_scene->addPixmap(QPixmap("imgs/Multijoueurs/background multijoueurs.png"))->setPos(QPointF(-1,-1));

    m_snakePosition.setX(430);
    m_snakePosition.setY(261);

    m_snakeAdvPosition.setX(40);
    m_snakeAdvPosition.setY(261);

    m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/rasse khdar left 3D.png")));
    m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos khdar left petit 3D.png")));
    m_snakeDirections.push_back(m_left);
    m_snakeDirections.push_back(m_left);

    m_snakeAdvImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/rasse hmar right 3D.png")));
    m_snakeAdvImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos hmar right petit 3D.png")));

    m_snakeImages.at(0)->setPos(m_snakePosition);
    m_snakeImages.at(1)->setPos(m_snakePosition.x()+30,m_snakePosition.y());

    m_snakeAdvImages.at(0)->setPos(m_snakeAdvPosition);
    m_snakeAdvImages.at(1)->setPos(m_snakeAdvPosition.x()-20,m_snakeAdvPosition.y());

    m_objectif = new QGraphicsPixmapItem;//pour charger la position de l'objectif
    m_objectif = m_scene->addPixmap(QPixmap("imgs/SnakeImage/Objectif.png"));
    m_chargerPositionObjectifTime = new QTimer(this);
    m_chargerPositionObjectifTime->setInterval(5000);

    m_tailleMsg = 0;
    m_objectifX = 0;
    m_objectifY = 0;
    m_typeMsg = 0;

    m_envoieSnakeTime = new QTimer(this);
    m_envoieSnakeTime->setInterval(100);
    m_envoieSnakeTime->start();

    m_pause = false;
    chargerObjectifPosition();
    m_chargerPositionObjectifTime->start();

    QObject::connect(m_client,SIGNAL(readyRead()),this,SLOT(donneesRecues()));
    QObject::connect(m_client,SIGNAL(disconnected()),this,SLOT(deconnexionClient()));
    QObject::connect(m_chargerPositionObjectifTime,SIGNAL(timeout()),this,SLOT(chargerObjectifPosition()));
    QObject::connect(m_envoieSnakeTime,SIGNAL(timeout()),this,SLOT(envoyerSnake()));
}

void SnakeMultijoueurs::deconnexionClient()
{
    emit snakeMultijoueursOut();
}

void SnakeMultijoueurs::deconnecter()
{
    m_client->disconnectFromHost();
    m_serveur->close();
    m_connecter->setEtat("Vous etes deconnecte..<br/>Veillez lancer un serveur<br/>ou vou connecter a un autre");
}
//------------ LE RESEAU

void SnakeMultijoueurs::chargerOptions()
{
    m_scores_options->beginGroup("options");
    m_options.push_back(key_num(m_scores_options->value("set_up").toString()));
    m_options.push_back(key_num(m_scores_options->value("set_down").toString()));
    m_options.push_back(key_num(m_scores_options->value("set_right").toString()));
    m_options.push_back(key_num(m_scores_options->value("set_left").toString()));
    m_options.push_back(key_num(m_scores_options->value("pause").toString()));
    m_options.push_back(key_num(m_scores_options->value("rejouer").toString()));
    m_options.push_back(key_num(m_scores_options->value("revenir_au_menu").toString()));
    m_options.push_back(key_num(m_scores_options->value("sortir_du_jeu").toString()));
    m_options.push_back(key_num(m_scores_options->value("son").toString()));
    m_scores_options->endGroup();
}

int SnakeMultijoueurs::key_num(QString key)const
{
    QChar lettre;
    lettre = key[0];
    if (key == "Up")
        return  Qt::Key_Up;
    else if (key == "Down")
        return  Qt::Key_Down;
    else if (key == "Right")
        return  Qt::Key_Right;
    else if (key == "Left")
        return Qt::Key_Left;
    else
        return lettre.unicode();
}

void SnakeMultijoueurs::rejouerSon(qint64 position)
{
    if (position == m_sonMultijoueurs->duration())
        m_sonMultijoueurs->play();
}

void SnakeMultijoueurs::snakeTailTranslation()
{
    for (int i(m_snakeImages.size()-1) ;i > 1 ;i--)//ON COPIE LE DEPLACEMENT DE TOUT LES TAILS VERS LES TAIL D'APRES apard la premiere
    {
        m_snakeDirections[i] = m_snakeDirections[i-1];
        m_snakeImages[i]->setPixmap(m_snakeImages[i-1]->pixmap());
        m_snakeImages[i]->setPos(m_snakeImages[i-1]->x(),m_snakeImages[i-1]->y());
    }

    /*on copie le deplacement de la tete vers la premiere tail*/
    m_snakeDirections[1] = m_snakeDirections[0];
    switch (m_snakeDirections[1])
    {
     case m_right:
        m_snakeImages[1]->setPixmap(QPixmap("imgs/SnakeImage/dos khdar right petit 3D.png"));
        m_snakeImages[1]->setPos(m_snakeImages[0]->x(),m_snakeImages[0]->y());
        break;
    case m_left:
        m_snakeImages[1]->setPixmap(QPixmap("imgs/SnakeImage/dos khdar left petit 3D.png"));
        m_snakeImages[1]->setPos(m_snakeImages[0]->x()+10,m_snakeImages[0]->y());
       break;
    case m_up:
        m_snakeImages[1]->setPixmap(QPixmap("imgs/SnakeImage/dos khdar up petit 3D.png"));
        m_snakeImages[1]->setPos(m_snakeImages[0]->x(),m_snakeImages[0]->y()+10);
       break;
    case m_down:
        m_snakeImages[1]->setPixmap(QPixmap("imgs/SnakeImage/dos khdar down petit 3D.png"));
        m_snakeImages[1]->setPos(m_snakeImages[0]->x(),m_snakeImages[0]->y());
       break;
    }

}

void SnakeMultijoueurs::ajouterTail()
{
    switch (m_snakeDirections[m_snakeDirections.size()-1])
    {
    case m_right:
        m_snakeDirections.push_back(m_right);
        m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos khdar right.png")));
        m_snakeImages[m_snakeImages.size()-1]->setPos(m_snakeImages[m_snakeImages.size()-2]->x()-20,m_snakeImages[m_snakeImages.size()-2]->y());
       break;
   case m_left:
        m_snakeDirections.push_back(m_left);
        m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos khdar left.png")));
        m_snakeImages[m_snakeImages.size()-1]->setPos(m_snakeImages[m_snakeImages.size()-2]->x()+20,m_snakeImages[m_snakeImages.size()-2]->y());
      break;
   case m_up:
        m_snakeDirections.push_back(m_up);
        m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos khdar up.png")));
        m_snakeImages[m_snakeImages.size()-1]->setPos(m_snakeImages[m_snakeImages.size()-2]->x(),m_snakeImages[m_snakeImages.size()-2]->y()+20);//en cas de regret +10 = +30
      break;
   case m_down:
        m_snakeDirections.push_back(m_down);
        m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos khdar down.png")));
        m_snakeImages[m_snakeImages.size()-1]->setPos(m_snakeImages[m_snakeImages.size()-2]->x(),m_snakeImages[m_snakeImages.size()-2]->y()-20);
      break;
    }

}

void SnakeMultijoueurs::eatingObjectif()
{
    switch (m_snakeDirections[0])
    {
    case m_right:
        if (m_snakePosition.x() + 20 == m_objectifX && m_snakePosition.y() - 1 == m_objectifY)
        {
            chargerObjectifPosition();
           ajouterTail();
           m_eatingObjSound->stop();
           m_eatingObjSound->play();
        }
        break;
    case m_left:
        if (m_snakePosition.x() - 10 == m_objectifX && m_snakePosition.y() - 1 == m_objectifY)
        {
           chargerObjectifPosition();
           ajouterTail();
           m_eatingObjSound->stop();
           m_eatingObjSound->play();
        }
        break;
    case m_up:
        if (m_snakePosition.x() -1 == m_objectifX && m_snakePosition.y() - 10 == m_objectifY)
        {
           chargerObjectifPosition();
           ajouterTail();
           m_eatingObjSound->stop();
           m_eatingObjSound->play();
        }
        break;
    case m_down:
        if (m_snakePosition.x() -1 == m_objectifX && m_snakePosition.y() + 20 == m_objectifY)
        {
           chargerObjectifPosition();
           ajouterTail();
           m_eatingObjSound->stop();
           m_eatingObjSound->play();
        }
        break;
    }

}

void SnakeMultijoueurs::gameOver()
{
    /* on verifie si la tete de croise pas un des tails*/
    switch (m_snakeDirections[0])
    {
    case m_right:
        for (int i(1) ;i < m_snakeImages.size() ;i++)
        {
            if (std::abs(m_snakePosition.x() + 30 - m_snakeImages[i]->x()) <= 10 && (m_snakePosition.x() + 30 - m_snakeImages[i]->x()) >= 0 && std::abs(m_snakePosition.y() - m_snakeImages[i]->y()) <= 1)
            {
                for (int j(i);j < m_snakeImages.size();j++)
                {
                    m_scene->removeItem(m_snakeImages[j]);
                }
                int tailleTail(m_snakeImages.size());
                for (int k(i);k < tailleTail;k++)
                {
                    m_snakeImages.removeLast();
                    m_snakeDirections.removeLast();
                }
                i=tailleTail;
            }
        }
       break;
    case m_left:
        for (int i(1) ;i < m_snakeImages.size() ;i++)
        {
            if (std::abs(m_snakePosition.x() - m_snakeImages[i]->x()) <= 10 && std::abs(m_snakePosition.y() - m_snakeImages[i]->y()) <= 1)
            {
                for (int j(i);j < m_snakeImages.size();j++)
                {
                    m_scene->removeItem(m_snakeImages[j]);
                }
                int tailleTail(m_snakeImages.size());
                for (int k(i);k < tailleTail;k++)
                {
                    m_snakeImages.removeLast();
                    m_snakeDirections.removeLast();
                }
                i=tailleTail;
            }
        }
       break;
    case m_up:
        for (int i(1) ;i < m_snakeImages.size() ;i++)
        {
            if (std::abs(m_snakePosition.x() - m_snakeImages[i]->x()) <= 1 && std::abs(m_snakePosition.y() - m_snakeImages[i]->y()) <= 10)
            {
                for (int j(i);j < m_snakeImages.size();j++)
                {
                    m_scene->removeItem(m_snakeImages[j]);
                }
                int tailleTail(m_snakeImages.size());
                for (int k(i);k < tailleTail;k++)
                {
                    m_snakeImages.removeLast();
                    m_snakeDirections.removeLast();
                }
                i=tailleTail;
            }
        }
       break;
    case m_down:
        for (int i(1) ;i < m_snakeImages.size() ;i++)
        {
            if (std::abs(m_snakePosition.x() - m_snakeImages[i]->x()) <= 1 && std::abs(m_snakePosition.y() + 20 - m_snakeImages[i]->y()) <= 10 && std::abs(m_snakePosition.y() + 20 - m_snakeImages[i]->y()) >= 0)
            {
                for (int j(i);j < m_snakeImages.size();j++)
                {
                    m_scene->removeItem(m_snakeImages[j]);
                }
                int tailleTail(m_snakeImages.size());
                for (int k(i);k < tailleTail;k++)
                {
                    m_snakeImages.removeLast();
                    m_snakeDirections.removeLast();
                }
                i=tailleTail;
            }
        }
       break;
    }

}

void SnakeMultijoueurs::theEndResult(int result)
{
    QByteArray paquetEnd;
    QDataStream out(&paquetEnd,QIODevice::WriteOnly);

    out << static_cast<quint16>(0);
    out << static_cast<quint8>(4);
    out << result;
    out.device()->seek(0);
    out << static_cast<quint16>(paquetEnd.size() - static_cast<int>(sizeof(quint16)) - static_cast<int>(sizeof(quint8)));

    m_client->write(paquetEnd);
}

SnakeMultijoueurs::~SnakeMultijoueurs()
{
    if (m_serveur->isListening())
    {
        m_client->disconnectFromHost();
        m_serveur->close();
    }
    else
    {
        m_client->disconnectFromHost();
    }

    if (rejouer)
    {
        emit newMultijoueurs(Ip,port,timer);
    }
}


//=============================================================================================================================
                            // LES SLOTS DU Snake Multijoueurs

void SnakeMultijoueurs::keyPressEvent(QKeyEvent *event)
{
    /*relancer ou stoper le son du jeu*/
    if (event->key() == m_options[8])
    {
        if (!m_sonMultijoueurs->isMuted())
            m_sonMultijoueurs->setMuted(true);
        else
            m_sonMultijoueurs->setMuted(false);
        return;
    }
    /* on verifie si c on est pas dans une pause et p est clique*/
    if (event->key() == m_options[4])
    {
        if (m_pauseImage->isVisible())//si on est dans une pause ou dans le game over
            return;
       m_chargerPositionSnakeTime->stop();
       m_pause = true;
       m_pauseOut = false;
       m_pauseImage->setPixmap(QPixmap("imgs/SnakeImage/Pause.png"));
       /*dessiner options*/
       QPixmap imgPause;
       imgPause = m_pauseImage->pixmap();
       m_scores_options->beginGroup("options");
       QPainter dessinOptions;
       dessinOptions.begin(&imgPause);
       dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
       dessinOptions.setFont(QFont("Impact",25));
       dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
       dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
       dessinOptions.setFont(QFont("Impact",12));
       dessinOptions.drawText(QPoint(353,275),m_scores_options->value("set_up").toString());
       dessinOptions.drawText(QPoint(353,350),m_scores_options->value("set_down").toString());
       dessinOptions.drawText(QPoint(407,325),m_scores_options->value("set_right").toString());
       dessinOptions.drawText(QPoint(290,325),m_scores_options->value("set_left").toString());
       dessinOptions.setPen(QColor(QColor::fromRgb(19,33,128)));
       dessinOptions.setFont(QFont("Impact",15));
       dessinOptions.drawText(QPoint(260,487),m_scores_options->value("son").toString());
       dessinOptions.end();
       m_pauseImage->setPixmap(imgPause);
       m_scores_options->endGroup();
       m_pauseImage->show();
       return;
    }

    /*si on est en pause et B est clique on revient au menu et on sauvgarde le score*/
    if (event->key() == m_options[6] && m_pauseImage->isVisible())
    {
       if (m_serveur->isListening())
       {
           m_client->disconnectFromHost();
           m_serveur->close();
       }
       else
       {
           m_client->disconnectFromHost();
       }
    }

    /*si on est en pause et Q est clique on quit le jeu et on sauvgarde le score*/
    if (event->key() == m_options[7] && m_pauseImage->isVisible())
    {
        if (m_serveur->isListening())
        {
            m_client->disconnectFromHost();
            m_serveur->close();
        }
        else
        {
            m_client->disconnectFromHost();
        }
        qApp->quit();
    }

    /*si on est en game over ou good game et R est clique on rejoue et on sauvgarde le score*/
    if (event->key() == m_options[5] && m_pauseImage->isVisible() && !m_pause)
    {
        //ON gere LE REJOUER partie
        rejouer = true;

        QByteArray paquetNew;
        QDataStream out(&paquetNew,QIODevice::WriteOnly);
        QString msg("rejouer");

        out << static_cast<quint16>(0);
        out << static_cast<quint8>(5);
        out << msg;
        out.device()->seek(0);
        out << static_cast<quint16>(paquetNew.size() - static_cast<int>(sizeof(quint16)) - static_cast<int>(sizeof(quint8)));
        m_client->write(paquetNew);
    }

    if (m_pauseImage->isVisible() && !m_pause)//si on est dans le game over ou good game on fait rien
        return;

    /*on verifie en cas meme direction et pause pour faire la simple translation*/
    if ((event->key() == m_options[2] && (m_snakeDirections.at(0) == m_right || m_snakeDirections.at(0) == m_left)) || (event->key() == m_options[3] && (m_snakeDirections.at(0) == m_left || m_snakeDirections.at(0) == m_right)) || (event->key() == m_options[0] && (m_snakeDirections.at(0) == m_up || m_snakeDirections.at(0) == m_down)) || (event->key() == m_options[1] && (m_snakeDirections.at(0) == m_down || m_snakeDirections.at(0) == m_up)))
    {
        if (!m_pauseOut && m_pause)
        {
        snakeTranslation();
        m_chargerPositionSnakeTime->start();
        }
    }

    /*si on clique sur aucun de ces boutons on fait un return*/
     if (m_pause)
    {
        if (event->key() != m_options[8] && event->key() != m_options[0] && event->key() != m_options[1] && event->key() != m_options[2] && event->key() != m_options[3])
            return;
    }

    /*on verifie si la direction clique n'est pas la direction actuel et le stepbystep est true et on change la position*/
                             /*on verifie si on mange l'objectif*/

    if (event->key() == m_options[0] && m_snakeDirections.at(0) == m_right)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar up 3D.png"));//on charge l'image
        if (m_snakePosition.x() == 480)//on verifie si on est pas endors de la fenetre
            m_snakePosition.setX(-20);
        m_snakePosition.setX(m_snakePosition.x()+21);//on fait la translation
        m_snakePosition.setY(m_snakePosition.y()-11);
        snakeTailTranslation();//pour bouger la queu
        m_snakeDirections[0] = m_up;//on change la direction
        m_chargerPositionSnakeTime->start();//on redemare le comptage
        eatingObjectif();//on verife si on mange pas l'objectif
        /*les autres c la mm chose*/
    }
    else if (event->key() == m_options[0] && m_snakeDirections.at(0) == m_left)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar up 3D.png"));
        if (m_snakePosition.x() == -10)
            m_snakePosition.setX(490);
        m_snakePosition.setX(m_snakePosition.x()-9);
        m_snakePosition.setY(m_snakePosition.y()-11);
        snakeTailTranslation();
        m_snakeDirections[0] = m_up;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }
    else if (event->key() == m_options[1] && m_snakeDirections.at(0) == m_right)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar down 3D.png"));
        if (m_snakePosition.x() == 480)
            m_snakePosition.setX(-20);
        m_snakePosition.setX(m_snakePosition.x()+21);
        m_snakePosition.setY(m_snakePosition.y()-1);
        snakeTailTranslation();
        m_snakeDirections[0] = m_down;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }
    else if (event->key() == m_options[1] && m_snakeDirections.at(0) == m_left)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar down 3D.png"));
        if (m_snakePosition.x() == -10)
            m_snakePosition.setX(490);
        m_snakePosition.setX(m_snakePosition.x()-9);
        m_snakePosition.setY(m_snakePosition.y()-1);
        snakeTailTranslation();
        m_snakeDirections[0] = m_down;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }
    else if (event->key() == m_options[2] && m_snakeDirections.at(0) == m_up)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar right 3D.png"));
        if (m_snakePosition.y() == -10)
            m_snakePosition.setY(490);
        m_snakePosition.setX(m_snakePosition.x()-1);
        m_snakePosition.setY(m_snakePosition.y()-9);
        snakeTailTranslation();
        m_snakeDirections[0] = m_right;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }
    else if (event->key() == m_options[2] && m_snakeDirections.at(0) == m_down)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar right 3D.png"));
        if (m_snakePosition.y() == 480)
            m_snakePosition.setY(-20);
        m_snakePosition.setX(m_snakePosition.x()-1);
        m_snakePosition.setY(m_snakePosition.y()+21);
        snakeTailTranslation();
        m_snakeDirections[0] = m_right;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }
    else if (event->key() == m_options[3] && m_snakeDirections.at(0) == m_up)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar left 3D.png"));
        if (m_snakePosition.y() == -10)
            m_snakePosition.setY(490);
        m_snakePosition.setX(m_snakePosition.x()-11);
        m_snakePosition.setY(m_snakePosition.y()-9);
        snakeTailTranslation();
        m_snakeDirections[0] = m_left;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }
    else if (event->key() == m_options[3] && m_snakeDirections.at(0) == m_down)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar left 3D.png"));
        if (m_snakePosition.y() == 480)
            m_snakePosition.setY(-20);
        m_snakePosition.setX(m_snakePosition.x()-11);
        m_snakePosition.setY(m_snakePosition.y()+21);
        snakeTailTranslation();
        m_snakeDirections[0] = m_left;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }

    /*on change la position de tout le snake*/
    m_snakeImages.at(0)->setPos(m_snakePosition);
    gameOver();//verification de game over

    if (m_pause)//si on etait dans une pause on relance le comptage normal et on cache le menu pause
    {
        m_pauseImage->hide();
        m_pauseOut = true;
    }
}

void SnakeMultijoueurs::chargerObjectifPosition()
{
    /*on positionne l'objectif avec precision*/
    m_objectifX = (rand() % 24) * 20;
    m_objectifY = (rand() % 24) * 20;

    QString msg,x,y;
    x.setNum(m_objectifX);
    y.setNum(m_objectifY);
    msg += x+"-"+y;
    //on verifie si on tombe pas sur la tete du snake

    QByteArray paquetObjPosition;
    QDataStream out(&paquetObjPosition,QIODevice::WriteOnly);

    out << static_cast<quint16>(0);
    out << static_cast<quint8>(1);
    out << msg;
    out.device()->seek(0);
    out << static_cast<quint16>(paquetObjPosition.size() - static_cast<int>(sizeof(quint16)) - static_cast<int>(sizeof(quint8)));

    m_client->write(paquetObjPosition);

    m_objectif->setPos(m_objectifX,m_objectifY);//on change la position de l'obj apres le calcul de la precision

    if (m_chargerPositionObjectifTime->isActive())
    {
        m_chargerPositionObjectifTime->start();
    }

    if (m_pause)//si on etait dans une pause on relance le comptage normal
    {
        m_pause = false;
    }

}

void SnakeMultijoueurs::snakeTranslation()
{
    switch (m_snakeDirections.at(0))//on effectue une translation selon la direction de la tete du snake et si on rencontre l'objectif on change la position de l'obj
    {
        case m_right:
         if (m_snakePosition.x() == 480)
             m_snakePosition.setX(-20);
         else
          m_snakePosition.setX(m_snakePosition.x()+20);
           eatingObjectif();
          break;
        case m_left:
        if (m_snakePosition.x() == -10)
            m_snakePosition.setX(490);
        else
          m_snakePosition.setX(m_snakePosition.x()-20);
            eatingObjectif();
          break;
        case m_up:
        if (m_snakePosition.y() == -10)
            m_snakePosition.setY(490);
        else
          m_snakePosition.setY(m_snakePosition.y()-20);
            eatingObjectif();
          break;
        case m_down:
        if (m_snakePosition.y() == 480)
            m_snakePosition.setY(-20);
        else
          m_snakePosition.setY(m_snakePosition.y()+20);
            eatingObjectif();
        break;
    }

    snakeTailTranslation();
    m_snakeImages.at(0)->setPos(m_snakePosition);
    gameOver();
}

void SnakeMultijoueurs::envoyerSnake()
{
    QByteArray paquetObjPosition;
    QDataStream out(&paquetObjPosition,QIODevice::WriteOnly);
    QString msg(""),x,y,direction;
    for (int i(0);i<m_snakeImages.size();i++)
    {
        direction.setNum(m_snakeDirections[i]);
        x.setNum(m_snakeImages[i]->x());
        y.setNum(m_snakeImages[i]->y());
        msg += direction + x +","+ y +"$";
    }

    out << static_cast<quint16>(0);
    out << static_cast<quint8>(2);
    out << msg;
    out.device()->seek(0);
    out << static_cast<quint16>(paquetObjPosition.size() - static_cast<int>(sizeof(quint16)) - static_cast<int>(sizeof(quint8)));

    m_client->write(paquetObjPosition);
}

void SnakeMultijoueurs::gameEnd()
{
    QByteArray paquetEnd;
    QDataStream out(&paquetEnd,QIODevice::WriteOnly);
    QString msg;
    msg.setNum(m_snakeImages.size());

    out << static_cast<quint16>(0);
    out << static_cast<quint8>(3);
    out << msg;
    out.device()->seek(0);
    out << static_cast<quint16>(paquetEnd.size() - static_cast<int>(sizeof(quint16)) - static_cast<int>(sizeof(quint8)));

    m_client->write(paquetEnd);

    m_sonMultijoueurs->stop();
    m_chargerPositionSnakeTime->stop();
    m_gameTime->stop();
}








