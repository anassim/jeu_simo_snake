#ifndef SNAKE_H
#define SNAKE_H

#include <QApplication>
#include <QObject>
#include <QMainWindow>
#include <QLabel>
#include <QEvent>
#include <QVBoxLayout>
#include <QStackedWidget>
#include "menusnake.h"
#include "snakesimplegame.h"
#include "snakemapgame.h"
#include "snakemultjoueurs.h"
#include "snakescores.h"
#include "snakeoptions.h"


class Snake :public QMainWindow //c'est la fenetre principale du jeu qui contiendra les autre widgets des modes
{
    Q_OBJECT

      public:
        Snake();


        ~Snake();

      protected slots:
        void menuSimpleGameCliqued();//pour entrer dans le simple jeu
        void SimpleGameOuted();//pour revenir au menu depuis le simple jeu
        void newSimpleGameCliqued();//pour rejouer la partie
        void menuMapGameCliqued(QString mapName);//pour entrer dans le map game
        void MapGameOuted();//pour revenir au menu depuis le map game
        void newMapGameCliqued(QString mapName);//pour rejouer la partie
        void menuMultijoueursCliqued();//pour entrer dans le map game
        void MultijoueursOuted();//pour revenir au menu depuis le map game
        void newMultijoueursCliqued(QString Ip,quint16 port,int timer);//pour rejouer la partie
        void menuSnakeOptionsCliqued();//pour entrer le menu options
        void snakeOptionsOuted();//pour revenir au menu
        void menuSnakeScoresCliqued();//pour entrer le menu scores
        void snakeScoresOuted();//pour revenir au menu

      signals:

      protected:
     int m_modeSnake;//pour le mode des widgets
     SnakeMenu *m_snakeMenu;// c'est pour le widget menu
     SnakeSimpleGame *m_snakeSimpleGame;// c'est pour le simple jeu
     SnakeMapGame *m_snakeMapGame;//c'est pour le map game
     SnakeMultijoueurs *m_snakeMultijoueurs;//c'est pour le mode multijoueurs
     SnakeScores *m_snakeScores;//c'est pour le widget snake score
     SnakeOptions *m_snakeOptions;//c'est pour le widget snake options
};

#endif // SNAKE_H
