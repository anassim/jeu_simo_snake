#include <QApplication>
#include <QtGui>
#include <QEvent>
#include <QCloseEvent>
#include <QPalette>
#include <QMessageBox>
#include "snake.h"
#include "menusnake.h"
#include "snakesimplegame.h"
#include "snakemultjoueurs.h"
#include "snakemapgame.h"
#include "snakescores.h"

//=============================================================================================================================
                            // LES METHODES DU Snake

Snake::Snake()
{
    /*on inisialise la fenetre principale avec le menu*/

    setWindowTitle("Simo_Snake");
    setWindowIcon(QIcon("imgs/MenuImage/Snake Icon.jpg"));
    setFixedSize(500,500);

    m_modeSnake = 1;
    m_snakeMenu = new SnakeMenu;

    setCentralWidget(m_snakeMenu);

    QObject::connect(m_snakeMenu,SIGNAL(menuSimpleGameClique()),this,SLOT(menuSimpleGameCliqued()));
    QObject::connect(m_snakeMenu,SIGNAL(menuMapGameClique(QString)),this,SLOT(menuMapGameCliqued(QString)));
    QObject::connect(m_snakeMenu,SIGNAL(menuMultijoueursClique()),this,SLOT(menuMultijoueursCliqued()));
    QObject::connect(m_snakeMenu,SIGNAL(menuSnakeScoresClique()),this,SLOT(menuSnakeScoresCliqued()));
    QObject::connect(m_snakeMenu,SIGNAL(menuSnakeOptionsClique()),this,SLOT(menuSnakeOptionsCliqued()));

}

Snake::~Snake()
{

}

//=============================================================================================================================
                            // LES SLOTS DU Snake

void Snake::menuSimpleGameCliqued()
{
    /*on suprime le widget actuel
            et on met le widget du simple jeu*/
    m_modeSnake  = 2;
    m_snakeSimpleGame = new SnakeSimpleGame;
    m_snakeMenu->deleteLater();

    setCentralWidget(m_snakeSimpleGame);
    QObject::connect(m_snakeSimpleGame,SIGNAL(snakeSimpleGameOut()),this,SLOT(SimpleGameOuted()));
    QObject::connect(m_snakeSimpleGame,SIGNAL(newSimpleGame()),this,SLOT(newSimpleGameCliqued()));
}

void Snake::SimpleGameOuted()
{
    /*on suprime le widget actuel
            et on met le widget du menu*/
   m_modeSnake = 1;
   m_snakeMenu = new SnakeMenu;
   m_snakeSimpleGame->deleteLater();
   setCentralWidget(m_snakeMenu);
   QObject::connect(m_snakeMenu,SIGNAL(menuSimpleGameClique()),this,SLOT(menuSimpleGameCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuMapGameClique(QString)),this,SLOT(menuMapGameCliqued(QString)));
   QObject::connect(m_snakeMenu,SIGNAL(menuMultijoueursClique()),this,SLOT(menuMultijoueursCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuSnakeScoresClique()),this,SLOT(menuSnakeScoresCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuSnakeOptionsClique()),this,SLOT(menuSnakeOptionsCliqued()));
}

void Snake::newSimpleGameCliqued()
{
    /*on suprime l'ancien widget de simple game
            et on met un nouveau widget du simple jeu*/
    m_modeSnake  = 2;
    m_snakeSimpleGame->deleteLater();
    m_snakeSimpleGame = new SnakeSimpleGame;
    setCentralWidget(m_snakeSimpleGame);
    QObject::connect(m_snakeSimpleGame,SIGNAL(newSimpleGame()),this,SLOT(newSimpleGameCliqued()));
    QObject::connect(m_snakeSimpleGame,SIGNAL(snakeSimpleGameOut()),this,SLOT(SimpleGameOuted()));
}

void Snake::menuMapGameCliqued(QString mapName)
{
    /*on suprime le widget actuel
            et on met le widget du map jeu*/
    m_modeSnake  = 3;
    m_snakeMapGame = new SnakeMapGame(mapName);
    m_snakeMenu->deleteLater();

    setCentralWidget(m_snakeMapGame);
    QObject::connect(m_snakeMapGame,SIGNAL(snakeMapGameOut()),this,SLOT(MapGameOuted()));
    QObject::connect(m_snakeMapGame,SIGNAL(newMapGame(QString)),this,SLOT(newMapGameCliqued(QString)));
}

void Snake::MapGameOuted()
{
    /*on suprime le widget actuel
            et on met le widget du menu*/
   m_modeSnake = 1;
   m_snakeMenu = new SnakeMenu;
   m_snakeMapGame->deleteLater();
   setCentralWidget(m_snakeMenu);
   QObject::connect(m_snakeMenu,SIGNAL(menuSimpleGameClique()),this,SLOT(menuSimpleGameCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuMapGameClique(QString)),this,SLOT(menuMapGameCliqued(QString)));
   QObject::connect(m_snakeMenu,SIGNAL(menuMultijoueursClique()),this,SLOT(menuMultijoueursCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuSnakeScoresClique()),this,SLOT(menuSnakeScoresCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuSnakeOptionsClique()),this,SLOT(menuSnakeOptionsCliqued()));
}

void Snake::newMapGameCliqued(QString mapName)
{
    /*on suprime l'ancien widget de map game
            et on met un nouveau widget du map jeu*/
    m_modeSnake  = 3;
    m_snakeMapGame->deleteLater();
    m_snakeMapGame = new SnakeMapGame(mapName);
    setCentralWidget(m_snakeMapGame);
    QObject::connect(m_snakeMapGame,SIGNAL(snakeMapGameOut()),this,SLOT(MapGameOuted()));
    QObject::connect(m_snakeMapGame,SIGNAL(newMapGame(QString)),this,SLOT(newMapGameCliqued(QString)));
}

void Snake::menuMultijoueursCliqued()
{
    /*on suprime le widget actuel
            et on met le widget du simple jeu*/
    m_modeSnake  = 4;
    m_snakeMultijoueurs = new SnakeMultijoueurs;
    m_snakeMenu->deleteLater();

    setCentralWidget(m_snakeMultijoueurs);
    QObject::connect(m_snakeMultijoueurs,SIGNAL(snakeMultijoueursOut()),this,SLOT(MultijoueursOuted()));
    QObject::connect(m_snakeMultijoueurs,SIGNAL(newMultijoueurs(QString,quint16,int)),this,SLOT(newMultijoueursCliqued(QString,quint16,int)));
}

void Snake::MultijoueursOuted()
{
    /*on suprime le widget actuel
            et on met le widget du menu*/
   m_modeSnake = 1;
   m_snakeMenu = new SnakeMenu;
   m_snakeMultijoueurs->deleteLater();
   setCentralWidget(m_snakeMenu);

   QObject::connect(m_snakeMenu,SIGNAL(menuSimpleGameClique()),this,SLOT(menuSimpleGameCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuMapGameClique(QString)),this,SLOT(menuMapGameCliqued(QString)));
   QObject::connect(m_snakeMenu,SIGNAL(menuMultijoueursClique()),this,SLOT(menuMultijoueursCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuSnakeScoresClique()),this,SLOT(menuSnakeScoresCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuSnakeOptionsClique()),this,SLOT(menuSnakeOptionsCliqued()));
}

void Snake::newMultijoueursCliqued(QString Ip,quint16 port,int timer)
{
    /*on suprime l'ancien widget de simple game
            et on met un nouveau widget du simple jeu*/
    m_modeSnake  = 4;
    m_snakeMultijoueurs->deleteLater();
    m_snakeMultijoueurs = new SnakeMultijoueurs(Ip,port,timer);
    setCentralWidget(m_snakeMultijoueurs);
    QObject::connect(m_snakeMultijoueurs,SIGNAL(newMultijoueurs(QString,quint16,int)),this,SLOT(newMultijoueursCliqued(QString,quint16,int)));
    QObject::connect(m_snakeMultijoueurs,SIGNAL(snakeMultijoueursOut()),this,SLOT(MultijoueursOuted()));
}

void Snake::menuSnakeOptionsCliqued()
{
    /*on suprime le widget actuel
            et on met le widget du snake options*/
    m_modeSnake  = 5;
    m_snakeOptions = new SnakeOptions(m_snakeMenu->getIndexHautParleur(),m_snakeMenu->getSonPosition());
    m_snakeMenu->deleteLater();

    setCentralWidget(m_snakeOptions);
    QObject::connect(m_snakeOptions,SIGNAL(snakeOptionsOut()),this,SLOT(snakeOptionsOuted()));
}

void Snake::snakeOptionsOuted()
{
    /*on suprime le widget actuel
            et on met le widget du options*/
   m_modeSnake = 1;
   m_snakeMenu = new SnakeMenu(m_snakeOptions->getIndexHautParleur(),m_snakeOptions->getSonPosition());
   m_snakeOptions->deleteLater();

   setCentralWidget(m_snakeMenu);
   QObject::connect(m_snakeMenu,SIGNAL(menuSimpleGameClique()),this,SLOT(menuSimpleGameCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuMapGameClique(QString)),this,SLOT(menuMapGameCliqued(QString)));
   QObject::connect(m_snakeMenu,SIGNAL(menuMultijoueursClique()),this,SLOT(menuMultijoueursCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuSnakeScoresClique()),this,SLOT(menuSnakeScoresCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuSnakeOptionsClique()),this,SLOT(menuSnakeOptionsCliqued()));
}

void Snake::menuSnakeScoresCliqued()
{
    /*on suprime le widget actuel
            et on met le widget du snake scores*/
    m_modeSnake  = 6;
    m_snakeScores = new SnakeScores(m_snakeMenu->getIndexHautParleur(),m_snakeMenu->getSonPosition());
    m_snakeMenu->deleteLater();
    setCentralWidget(m_snakeScores);
    QObject::connect(m_snakeScores,SIGNAL(snakeScoresOut()),this,SLOT(snakeScoresOuted()));
}

void Snake::snakeScoresOuted()
{
    /*on suprime le widget actuel
            et on met le widget du scores*/
   m_modeSnake = 1;
   m_snakeMenu = new SnakeMenu(m_snakeScores->getIndexHautParleur(),m_snakeScores->getSonPosition());
   m_snakeScores->deleteLater();

   setCentralWidget(m_snakeMenu);
   QObject::connect(m_snakeMenu,SIGNAL(menuSimpleGameClique()),this,SLOT(menuSimpleGameCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuMapGameClique(QString)),this,SLOT(menuMapGameCliqued(QString)));
   QObject::connect(m_snakeMenu,SIGNAL(menuMultijoueursClique()),this,SLOT(menuMultijoueursCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuSnakeScoresClique()),this,SLOT(menuSnakeScoresCliqued()));
   QObject::connect(m_snakeMenu,SIGNAL(menuSnakeOptionsClique()),this,SLOT(menuSnakeOptionsCliqued()));
}


