#include <QApplication>
#include <QObject>
#include <QMainWindow>
#include <QLabel>
#include <QEvent>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsProxyWidget>
#include <QGraphicsPixmapItem>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QCloseEvent>
#include <QTimer>
#include <QTime>
#include <QList>
#include <QMediaPlayer>
#include <cmath>
#include <sstream>
#include "snakesimplegame.h"

enum direction{m_up,m_down,m_right,m_left};

//=============================================================================================================================
                            // LES METHODES DU Snake Simple Game

SnakeSimpleGame::SnakeSimpleGame()
{
    /*inisialisation des options du simple game*/
    m_scores_options = new QSettings("scores_options/scores_options.ini",QSettings::IniFormat,this);
    chargerOptions();

    /* inisialisation de la scene du simple jeu*/
    m_sonSimpleGame = new QMediaPlayer(this);
    m_sonSimpleGame->setMedia(QUrl("sons/son simple game.mp3"));
    m_sonSimpleGame->setVolume(60);
    m_sonSimpleGame->play();

    m_scene = new QGraphicsScene;
    m_scene->setSceneRect(0,0,500,500);
    m_scene->addPixmap(QPixmap("imgs/SnakeImage/background simple game.png"))->setPos(QPointF(-1,-1));

    m_objectif = new QGraphicsPixmapItem;
    m_objectif = m_scene->addPixmap(QPixmap("imgs/SnakeImage/Objectif.png"));
    m_pause = false;
    m_chargerPositionObjectifTime = new QTimer(this);
    m_chargerPositionObjectifTime->setInterval(5000);
    m_chargerPositionObjectifTime->start();

    m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/rasse khdar right 3D.png")));
    m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos khdar right petit 3D.png")));
    m_snakeDirections.push_back(m_right);
    m_snakeDirections.push_back(m_right);
    chargerSnakePosition();
    chargerObjectifPosition();
    m_chargerPositionSnakeTime = new QTimer(this);
    m_chargerPositionSnakeTime->setInterval(90);
    m_chargerPositionSnakeTime->start();

    m_pauseImage = new QGraphicsPixmapItem;
    m_pauseImage = m_scene->addPixmap(QPixmap("imgs/SnakeImage/Pause.png"));
    m_pauseImage->setPos(0,0);
    m_pauseImage->setZValue(4);
    m_pauseImage->hide();

    m_pauseOut = true;

    m_eatingObjSound = new QMediaPlayer(this);
    m_eatingObjSound->setMedia(QUrl("sons/eatingObjectif.mp3"));

    m_sonGameOver = new QMediaPlayer(this);
    m_sonGameOver->setMedia(QUrl("sons/game over.mp3"));
    m_sonGameOver->stop();
    m_sonGoodGame = new QMediaPlayer(this);
    m_sonGoodGame->setMedia(QUrl("sons/good game.mp3"));

    m_scores_options->beginGroup("scores");
    m_listeScores = m_scores_options->allKeys();
    m_afficherScore = new QGraphicsTextItem;
    meilleureScore = m_scores_options->value(m_listeScores[0]).toInt();
    m_scores_options->endGroup();

    setScene(m_scene);
    setFixedSize(502,502);//fixer la taille a 502 pour ne pas avoir a resizer la scene
    setRenderHint(QPainter::Antialiasing);
    grabKeyboard();//on fait le focus des keypress sur ce widget

    QObject::connect(m_chargerPositionObjectifTime,SIGNAL(timeout()),this,SLOT(chargerObjectifPosition()));
    QObject::connect(m_chargerPositionSnakeTime,SIGNAL(timeout()),this,SLOT(snakeTranslation()));
    QObject::connect(m_sonSimpleGame,SIGNAL(positionChanged(qint64)),this,SLOT(rejouerSon(qint64)));
}

void SnakeSimpleGame::chargerOptions()
{
    m_scores_options->beginGroup("options");
    m_options.push_back(key_num(m_scores_options->value("set_up").toString()));
    m_options.push_back(key_num(m_scores_options->value("set_down").toString()));
    m_options.push_back(key_num(m_scores_options->value("set_right").toString()));
    m_options.push_back(key_num(m_scores_options->value("set_left").toString()));
    m_options.push_back(key_num(m_scores_options->value("pause").toString()));
    m_options.push_back(key_num(m_scores_options->value("rejouer").toString()));
    m_options.push_back(key_num(m_scores_options->value("revenir_au_menu").toString()));
    m_options.push_back(key_num(m_scores_options->value("sortir_du_jeu").toString()));
    m_options.push_back(key_num(m_scores_options->value("son").toString()));
    m_scores_options->endGroup();
}

int SnakeSimpleGame::key_num(QString key)const
{
    QChar lettre;
    lettre = key[0];
    if (key == "Up")
        return  Qt::Key_Up;
    else if (key == "Down")
        return  Qt::Key_Down;
    else if (key == "Right")
        return  Qt::Key_Right;
    else if (key == "Left")
        return Qt::Key_Left;
    else
        return lettre.unicode();
}

void SnakeSimpleGame::rejouerSon(qint64 position)
{
    if (position == m_sonSimpleGame->duration())
        m_sonSimpleGame->play();
}

void SnakeSimpleGame::chargerSnakePosition()
{
       /*on choisis un nombre aleatoire en le multipliant a 20 (+1 en cas de y) on trouve la position de snake*/
        m_snakePosition.setX((rand() % 24) * 20);
        m_snakePosition.setY((rand() % 24) * 20 + 1);

    m_snakeImages.at(0)->setPos(m_snakePosition);
    m_snakeImages.at(1)->setPos(m_snakePosition.x()-20,m_snakePosition.y());
}

void SnakeSimpleGame::snakeTailTranslation()
{
    for (int i(m_snakeImages.size()-1) ;i > 1 ;i--)//ON COPIE LE DEPLACEMENT DE TOUT LES TAILS VERS LES TAIL D'APRES apard la premiere
    {
        m_snakeDirections[i] = m_snakeDirections[i-1];
        m_snakeImages[i]->setPixmap(m_snakeImages[i-1]->pixmap());
        m_snakeImages[i]->setPos(m_snakeImages[i-1]->x(),m_snakeImages[i-1]->y());
    }

    /*on copie le deplacement de la tete vers la premiere tail*/
    m_snakeDirections[1] = m_snakeDirections[0];
    switch (m_snakeDirections[1])
    {
     case m_right:
        m_snakeImages[1]->setPixmap(QPixmap("imgs/SnakeImage/dos khdar right petit 3D.png"));
        m_snakeImages[1]->setPos(m_snakeImages[0]->x(),m_snakeImages[0]->y());
        break;
    case m_left:
        m_snakeImages[1]->setPixmap(QPixmap("imgs/SnakeImage/dos khdar left petit 3D.png"));
        m_snakeImages[1]->setPos(m_snakeImages[0]->x()+10,m_snakeImages[0]->y());
       break;
    case m_up:
        m_snakeImages[1]->setPixmap(QPixmap("imgs/SnakeImage/dos khdar up petit 3D.png"));
        m_snakeImages[1]->setPos(m_snakeImages[0]->x(),m_snakeImages[0]->y()+10);
       break;
    case m_down:
        m_snakeImages[1]->setPixmap(QPixmap("imgs/SnakeImage/dos khdar down petit 3D.png"));
        m_snakeImages[1]->setPos(m_snakeImages[0]->x(),m_snakeImages[0]->y());
       break;
    }

}

void SnakeSimpleGame::ajouterTail()
{
    switch (m_snakeDirections[m_snakeDirections.size()-1])
    {
    case m_right:
        m_snakeDirections.push_back(m_right);
        m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos khdar right.png")));
        m_snakeImages[m_snakeImages.size()-1]->setPos(m_snakeImages[m_snakeImages.size()-2]->x()-20,m_snakeImages[m_snakeImages.size()-2]->y());
       break;
   case m_left:
        m_snakeDirections.push_back(m_left);
        m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos khdar left.png")));
        m_snakeImages[m_snakeImages.size()-1]->setPos(m_snakeImages[m_snakeImages.size()-2]->x()+20,m_snakeImages[m_snakeImages.size()-2]->y());
      break;
   case m_up:
        m_snakeDirections.push_back(m_up);
        m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos khdar up.png")));
        m_snakeImages[m_snakeImages.size()-1]->setPos(m_snakeImages[m_snakeImages.size()-2]->x(),m_snakeImages[m_snakeImages.size()-2]->y()+20);
      break;
   case m_down:
        m_snakeDirections.push_back(m_down);
        m_snakeImages.push_back(m_scene->addPixmap(QPixmap("imgs/SnakeImage/dos khdar down.png")));
        m_snakeImages[m_snakeImages.size()-1]->setPos(m_snakeImages[m_snakeImages.size()-2]->x(),m_snakeImages[m_snakeImages.size()-2]->y()-20);
      break;
    }

}

void SnakeSimpleGame::eatingObjectif()
{
    switch (m_snakeDirections[0])
    {
    case m_right:
        if (m_snakePosition.x() + 20 == m_objectifX && m_snakePosition.y() - 1 == m_objectifY)
        {
           chargerObjectifPosition();
           m_chargerPositionObjectifTime->start();
           ajouterTail();
           m_eatingObjSound->stop();
           m_eatingObjSound->play();
        }
        break;
    case m_left:
        if (m_snakePosition.x() - 10 == m_objectifX && m_snakePosition.y() - 1 == m_objectifY)
        {
           chargerObjectifPosition();
           m_chargerPositionObjectifTime->start();
           ajouterTail();
           m_eatingObjSound->stop();
           m_eatingObjSound->play();
        }
        break;
    case m_up:
        if (m_snakePosition.x() -1 == m_objectifX && m_snakePosition.y() - 10 == m_objectifY)
        {
           chargerObjectifPosition();
           m_chargerPositionObjectifTime->start();
           ajouterTail();
           m_eatingObjSound->stop();
           m_eatingObjSound->play();
        }
        break;
    case m_down:
        if (m_snakePosition.x() -1 == m_objectifX && m_snakePosition.y() + 20 == m_objectifY)
        {
           chargerObjectifPosition();
           m_chargerPositionObjectifTime->start();
           ajouterTail();
           m_eatingObjSound->stop();
           m_eatingObjSound->play();
        }
        break;
    }
}

void SnakeSimpleGame::gameOver()
{
    QString score;
    score.setNum(m_snakeImages.size());
    /* on verifie si la tete de croise pas un des tails*/
    switch (m_snakeDirections[0])
    {
    case m_right:
        for (int i(1) ;i < m_snakeImages.size() ;i++)
        {
            if (std::abs(m_snakePosition.x() + 30 - m_snakeImages[i]->x()) <= 10 && (m_snakePosition.x() + 30 - m_snakeImages[i]->x()) >= 0 && std::abs(m_snakePosition.y() - m_snakeImages[i]->y()) <= 1)
            {
                m_chargerPositionSnakeTime->stop();
                m_chargerPositionObjectifTime->stop();
                m_sonSimpleGame->stop();
                if (m_snakeImages.size() <= meilleureScore)
                {
                    m_pauseImage->setPixmap(QPixmap("imgs/SnakeImage/Game Over.png"));
                    /*ecrire les options*/
                    imagePause = m_pauseImage->pixmap();
                    m_scores_options->beginGroup("options");
                    QPainter dessinOptions;
                    dessinOptions.begin(&imagePause);
                    dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
                    dessinOptions.setFont(QFont("Impact",25));
                    dessinOptions.drawText(QPoint(353,335),m_scores_options->value("rejouer").toString());
                    dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
                    dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
                    dessinOptions.end();
                    m_pauseImage->setPixmap(imagePause);
                    m_scores_options->endGroup();
                    m_sonGameOver->play();
                }
                else if (m_snakeImages.size() > meilleureScore)//on depassant le meilleure score
                {
                    m_pauseImage->setPixmap(QPixmap("imgs/SnakeImage/Good Game.png"));
                    m_afficherScore = m_scene->addText(score);
                    m_afficherScore->setPos(290,120);
                    m_afficherScore->setFont(QFont("Lucida Bright",17,Qt::black));
                    m_afficherScore->setZValue(5);
                    /*ecrire les options*/
                    imagePause = m_pauseImage->pixmap();
                    m_scores_options->beginGroup("options");
                    QPainter dessinOptions;
                    dessinOptions.begin(&imagePause);
                    dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
                    dessinOptions.setFont(QFont("Impact",25));
                    dessinOptions.drawText(QPoint(353,335),m_scores_options->value("rejouer").toString());
                    dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
                    dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
                    dessinOptions.end();
                    m_pauseImage->setPixmap(imagePause);
                    m_scores_options->endGroup();
                    m_sonGoodGame->play();
                }
                m_pauseImage->show();
            }
        }
       break;
    case m_left:
        for (int i(1) ;i < m_snakeImages.size() ;i++)
        {
            if (std::abs(m_snakePosition.x() - m_snakeImages[i]->x()) <= 10 && std::abs(m_snakePosition.y() - m_snakeImages[i]->y()) <= 1)
            {
                m_chargerPositionSnakeTime->stop();
                m_chargerPositionObjectifTime->stop();
                m_sonSimpleGame->stop();
                if (m_snakeImages.size() <= meilleureScore)
                {
                    m_pauseImage->setPixmap(QPixmap("imgs/SnakeImage/Game Over.png"));
                    /*ecrire les options*/
                    imagePause = m_pauseImage->pixmap();
                    m_scores_options->beginGroup("options");
                    QPainter dessinOptions;
                    dessinOptions.begin(&imagePause);
                    dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
                    dessinOptions.setFont(QFont("Impact",25));
                    dessinOptions.drawText(QPoint(353,335),m_scores_options->value("rejouer").toString());
                    dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
                    dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
                    dessinOptions.end();
                    m_pauseImage->setPixmap(imagePause);
                    m_scores_options->endGroup();
                    m_sonGameOver->play();
                }
                else if (m_snakeImages.size() > meilleureScore)//on depassant le meilleure score
                {
                    m_pauseImage->setPixmap(QPixmap("imgs/SnakeImage/Good Game.png"));
                    m_afficherScore = m_scene->addText(score);
                    m_afficherScore->setPos(290,120);
                    m_afficherScore->setFont(QFont("Lucida Bright",17,Qt::black));
                    m_afficherScore->setZValue(5);
                    /*ecrire les options*/
                    imagePause = m_pauseImage->pixmap();
                    m_scores_options->beginGroup("options");
                    QPainter dessinOptions;
                    dessinOptions.begin(&imagePause);
                    dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
                    dessinOptions.setFont(QFont("Impact",25));
                    dessinOptions.drawText(QPoint(353,335),m_scores_options->value("rejouer").toString());
                    dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
                    dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
                    dessinOptions.end();
                    m_pauseImage->setPixmap(imagePause);
                    m_scores_options->endGroup();
                    m_sonGoodGame->play();
                }
                m_pauseImage->show();
            }
        }
       break;
    case m_up:
        for (int i(1) ;i < m_snakeImages.size() ;i++)
        {
            if (std::abs(m_snakePosition.x() - m_snakeImages[i]->x()) <= 1 && std::abs(m_snakePosition.y() - m_snakeImages[i]->y()) <= 10)
            {
                m_chargerPositionSnakeTime->stop();
                m_chargerPositionObjectifTime->stop();
                m_sonSimpleGame->stop();
                if (m_snakeImages.size() <= meilleureScore)
                {
                    m_pauseImage->setPixmap(QPixmap("imgs/SnakeImage/Game Over.png"));
                    /*ecrire les options*/
                    imagePause = m_pauseImage->pixmap();
                    m_scores_options->beginGroup("options");
                    QPainter dessinOptions;
                    dessinOptions.begin(&imagePause);
                    dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
                    dessinOptions.setFont(QFont("Impact",25));
                    dessinOptions.drawText(QPoint(353,335),m_scores_options->value("rejouer").toString());
                    dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
                    dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
                    dessinOptions.end();
                    m_pauseImage->setPixmap(imagePause);
                    m_scores_options->endGroup();
                    m_sonGameOver->play();
                }
                else if (m_snakeImages.size() > meilleureScore)//on depassant le meilleure score
                {
                    m_pauseImage->setPixmap(QPixmap("imgs/SnakeImage/Good Game.png"));
                    m_afficherScore = m_scene->addText(score);
                    m_afficherScore->setPos(290,120);
                    m_afficherScore->setFont(QFont("Lucida Bright",17,Qt::black));
                    m_afficherScore->setZValue(5);
                    /*ecrire les options*/
                    imagePause = m_pauseImage->pixmap();
                    m_scores_options->beginGroup("options");
                    QPainter dessinOptions;
                    dessinOptions.begin(&imagePause);
                    dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
                    dessinOptions.setFont(QFont("Impact",25));
                    dessinOptions.drawText(QPoint(353,335),m_scores_options->value("rejouer").toString());
                    dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
                    dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
                    dessinOptions.end();
                    m_pauseImage->setPixmap(imagePause);
                    m_scores_options->endGroup();
                    m_sonGoodGame->play();
                }
                m_pauseImage->show();
            }
        }
       break;
    case m_down:
        for (int i(1) ;i < m_snakeImages.size() ;i++)
        {
            if (std::abs(m_snakePosition.x() - m_snakeImages[i]->x()) <= 1 && std::abs(m_snakePosition.y() + 20 - m_snakeImages[i]->y()) <= 10 && std::abs(m_snakePosition.y() + 20 - m_snakeImages[i]->y()) >= 0)
            {
                m_chargerPositionSnakeTime->stop();
                m_chargerPositionObjectifTime->stop();
                m_sonSimpleGame->stop();
                if (m_snakeImages.size() <= meilleureScore)
                {
                    m_pauseImage->setPixmap(QPixmap("imgs/SnakeImage/Game Over.png"));
                    /*ecrire les options*/
                    imagePause = m_pauseImage->pixmap();
                    m_scores_options->beginGroup("options");
                    QPainter dessinOptions;
                    dessinOptions.begin(&imagePause);
                    dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
                    dessinOptions.setFont(QFont("Impact",25));
                    dessinOptions.drawText(QPoint(353,335),m_scores_options->value("rejouer").toString());
                    dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
                    dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
                    dessinOptions.end();
                    m_pauseImage->setPixmap(imagePause);
                    m_scores_options->endGroup();
                    m_sonGameOver->play();
                }
                else if (m_snakeImages.size() > meilleureScore)//on depassant le meilleure score
                {
                    m_pauseImage->setPixmap(QPixmap("imgs/SnakeImage/Good Game.png"));
                    m_afficherScore = m_scene->addText(score);
                    m_afficherScore->setPos(290,120);
                    m_afficherScore->setFont(QFont("Lucida Bright",17,Qt::black));
                    m_afficherScore->setZValue(5);
                    /*ecrire les options*/

                    imagePause = m_pauseImage->pixmap();
                    m_scores_options->beginGroup("options");
                    QPainter dessinOptions;
                    dessinOptions.begin(&imagePause);
                    dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
                    dessinOptions.setFont(QFont("Impact",25));
                    dessinOptions.drawText(QPoint(353,335),m_scores_options->value("rejouer").toString());
                    dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
                    dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
                    dessinOptions.end();
                    m_pauseImage->setPixmap(imagePause);
                    m_scores_options->endGroup();
                    m_sonGoodGame->play();
                }
                m_pauseImage->show();
            }
        }
       break;
    }

}

void SnakeSimpleGame::sauvgarderScore()
{
    m_scores_options->beginGroup("scores");
    for (int i(0);i < m_listeScores.size();i++)
    {
        if (m_snakeImages.size() == m_scores_options->value(m_listeScores[i]).toInt())
            return;
        else if (m_snakeImages.size() > m_scores_options->value(m_listeScores[i]).toInt())
        {
            for (int j(4);j > i;j--)
            {
                m_scores_options->setValue(m_listeScores[j],m_scores_options->value(m_listeScores[j-1]).toInt());
            }
            m_scores_options->setValue(m_listeScores[i],m_snakeImages.size());
            i = m_listeScores.size();
        }
    }
    m_scores_options->endGroup();
}

SnakeSimpleGame::~SnakeSimpleGame()
{
    sauvgarderScore();
}


//=============================================================================================================================
                            // LES SLOTS DU Snake Simple Game

void SnakeSimpleGame::keyPressEvent(QKeyEvent *event)
{
    /*relancer ou stoper le son du jeu*/
    if (event->key() == m_options[8])
    {
        if (!m_sonSimpleGame->isMuted())
            m_sonSimpleGame->setMuted(true);
        else
            m_sonSimpleGame->setMuted(false);
        return;
    }
    /* on verifie si c on est pas dans une pause et p est clique*/
    if (event->key() == m_options[4])
    {
        if (m_pauseImage->isVisible())//si on est dans une pause ou dans le game over
            return;
       m_chargerPositionSnakeTime->stop();
       m_chargerPositionObjectifTime->stop();
       m_pause = true;
       m_pauseOut = false;
       m_pauseImage->setPixmap(QPixmap("imgs/SnakeImage/Pause.png"));
       /*dessiner options*/
       QPixmap imgPause;
       imgPause = m_pauseImage->pixmap();
       m_scores_options->beginGroup("options");
       QPainter dessinOptions;
       dessinOptions.begin(&imgPause);
       dessinOptions.setPen(QColor(QColor::fromRgb(15,90,11)));
       dessinOptions.setFont(QFont("Impact",25));
       dessinOptions.drawText(QPoint(353,390),m_scores_options->value("revenir_au_menu").toString());
       dessinOptions.drawText(QPoint(353,437),m_scores_options->value("sortir_du_jeu").toString());
       dessinOptions.setFont(QFont("Impact",12));
       dessinOptions.drawText(QPoint(353,275),m_scores_options->value("set_up").toString());
       dessinOptions.drawText(QPoint(353,350),m_scores_options->value("set_down").toString());
       dessinOptions.drawText(QPoint(407,325),m_scores_options->value("set_right").toString());
       dessinOptions.drawText(QPoint(290,325),m_scores_options->value("set_left").toString());
       dessinOptions.setPen(QColor(QColor::fromRgb(19,33,128)));
       dessinOptions.setFont(QFont("Impact",15));
       dessinOptions.drawText(QPoint(260,487),m_scores_options->value("son").toString());
       dessinOptions.end();
       m_pauseImage->setPixmap(imgPause);
       m_scores_options->endGroup();
       m_pauseImage->show();
       return;
    }

    /*si on est en pause et B est clique on revient au menu et on sauvgarde le score*/
    if (event->key() == m_options[6] && m_pauseImage->isVisible())
    {
       emit snakeSimpleGameOut();
    }

    /*si on est en pause et Q est clique on quit le jeu et on sauvgarde le score*/
    if (event->key() == m_options[7] && m_pauseImage->isVisible())
    {
        qApp->quit();
    }

    /*si on est en game over ou good game et R est clique on rejoue et on sauvgarde le score*/
    if (event->key() == m_options[5] && m_pauseImage->isVisible() && !m_pause)
    {
        emit newSimpleGame();
    }

    if (m_pauseImage->isVisible() && !m_pause)//si on est dans le game over ou good game on fait rien
        return;

    /*on verifie en cas meme direction et pause pour faire la simple translation*/
    if ((event->key() == m_options[2] && (m_snakeDirections.at(0) == m_right || m_snakeDirections.at(0) == m_left)) || (event->key() == m_options[3] && (m_snakeDirections.at(0) == m_left || m_snakeDirections.at(0) == m_right)) || (event->key() == m_options[0] && (m_snakeDirections.at(0) == m_up || m_snakeDirections.at(0) == m_down)) || (event->key() == m_options[1] && (m_snakeDirections.at(0) == m_down || m_snakeDirections.at(0) == m_up)))
    {
        if (!m_pauseOut && m_pause)
        {
        snakeTranslation();
        m_chargerPositionSnakeTime->start();
        }
    }

    /*si on clique sur aucun de ces boutons on fait un return*/
     if (m_pause)
    {
        if (event->key() != m_options[8] && event->key() != m_options[0] && event->key() != m_options[1] && event->key() != m_options[2] && event->key() != m_options[3])
            return;
    }

    /*on verifie si la direction clique n'est pas la direction actuel et le stepbystep est true et on change la position*/
                             /*on verifie si on mange l'objectif*/

    if (event->key() == m_options[0] && m_snakeDirections.at(0) == m_right)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar up 3D.png"));//on charge l'image
        if (m_snakePosition.x() == 480)//on verifie si on est pas endors de la fenetre
            m_snakePosition.setX(-20);
        m_snakePosition.setX(m_snakePosition.x()+21);//on fait la translation
        m_snakePosition.setY(m_snakePosition.y()-11);
        snakeTailTranslation();//pour bouger la queu
        m_snakeDirections[0] = m_up;//on change la direction
        m_chargerPositionSnakeTime->start();//on redemare le comptage de la translation du snake
        eatingObjectif();//on verife si on mange pas l'objectif
        /*les autres c la mm chose*/
    }
    else if (event->key() == m_options[0] && m_snakeDirections.at(0) == m_left)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar up 3D.png"));
        if (m_snakePosition.x() == -10)
            m_snakePosition.setX(490);
        m_snakePosition.setX(m_snakePosition.x()-9);
        m_snakePosition.setY(m_snakePosition.y()-11);
        snakeTailTranslation();
        m_snakeDirections[0] = m_up;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }
    else if (event->key() == m_options[1] && m_snakeDirections.at(0) == m_right)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar down 3D.png"));
        if (m_snakePosition.x() == 480)
            m_snakePosition.setX(-20);
        m_snakePosition.setX(m_snakePosition.x()+21);
        m_snakePosition.setY(m_snakePosition.y()-1);
        snakeTailTranslation();
        m_snakeDirections[0] = m_down;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }
    else if (event->key() == m_options[1] && m_snakeDirections.at(0) == m_left)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar down 3D.png"));
        if (m_snakePosition.x() == -10)
            m_snakePosition.setX(490);
        m_snakePosition.setX(m_snakePosition.x()-9);
        m_snakePosition.setY(m_snakePosition.y()-1);
        snakeTailTranslation();
        m_snakeDirections[0] = m_down;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }
    else if (event->key() == m_options[2] && m_snakeDirections.at(0) == m_up)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar right 3D.png"));
        if (m_snakePosition.y() == -10)
            m_snakePosition.setY(490);
        m_snakePosition.setX(m_snakePosition.x()-1);
        m_snakePosition.setY(m_snakePosition.y()-9);
        snakeTailTranslation();
        m_snakeDirections[0] = m_right;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }
    else if (event->key() == m_options[2] && m_snakeDirections.at(0) == m_down)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar right 3D.png"));
        if (m_snakePosition.y() == 480)
            m_snakePosition.setY(-20);
        m_snakePosition.setX(m_snakePosition.x()-1);
        m_snakePosition.setY(m_snakePosition.y()+21);
        snakeTailTranslation();
        m_snakeDirections[0] = m_right;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }
    else if (event->key() == m_options[3] && m_snakeDirections.at(0) == m_up)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar left 3D.png"));
        if (m_snakePosition.y() == -10)
            m_snakePosition.setY(490);
        m_snakePosition.setX(m_snakePosition.x()-11);
        m_snakePosition.setY(m_snakePosition.y()-9);
        snakeTailTranslation();
        m_snakeDirections[0] = m_left;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }
    else if (event->key() == m_options[3] && m_snakeDirections.at(0) == m_down)
    {
        m_snakeImages.at(0)->setPixmap(QPixmap("imgs/SnakeImage/rasse khdar left 3D.png"));
        if (m_snakePosition.y() == 480)
            m_snakePosition.setY(-20);
        m_snakePosition.setX(m_snakePosition.x()-11);
        m_snakePosition.setY(m_snakePosition.y()+21);
        snakeTailTranslation();
        m_snakeDirections[0] = m_left;
        m_chargerPositionSnakeTime->start();
        eatingObjectif();
    }

    /*on change la position de tout le snake*/
    m_snakeImages.at(0)->setPos(m_snakePosition);
    gameOver();//verification de game over

    if (m_pause)//si on etait dans une pause on relance le comptage normal et on cache le menu pause
    {
        m_chargerPositionObjectifTime->start();
        m_pauseImage->hide();
        chargerObjectifPosition();
        m_pauseOut = true;
    }
    QObject::connect(m_chargerPositionObjectifTime,SIGNAL(timeout()),this,SLOT(chargerObjectifPosition()));
}

void SnakeSimpleGame::chargerObjectifPosition()
{
    /*on positionne l'objectif avec precision*/
    m_objectifX = (rand() % 24) * 20;
    m_objectifY = (rand() % 24) * 20;

   //on verifie si on tombe pas sur la tete du snake

    recalculer:

    switch (m_snakeDirections[0])
    {
    case m_right:
        if ((m_snakePosition.x() - m_objectifX == 0 || m_snakePosition.x() - m_objectifX == -20) && (m_snakePosition.y() - m_objectifY == 1))
        {
                m_objectifX = (rand() % 24) * 20;
                m_objectifY = (rand() % 24) * 20;
             goto recalculer;
        }
     break;
    case m_left:
        if ((m_snakePosition.x() - m_objectifX == -10 || m_snakePosition.x() - m_objectifX == 10) && (m_snakePosition.y() - m_objectifY == 1))
        {
                m_objectifX = (rand() % 24) * 20;
                m_objectifY = (rand() % 24) * 20;
            goto recalculer;
        }
     break;
    case m_up:
        if ((m_snakePosition.y() - m_objectifY == -10 || m_snakePosition.y() - m_objectifY == 10) && (m_snakePosition.x() - m_objectifX == 1))
        {
                m_objectifX = (rand() % 24) * 20;
                m_objectifY = (rand() % 24) * 20;
            goto recalculer;
        }
     break;
    case m_down:
        if ((m_snakePosition.y() - m_objectifY == 0 || m_snakePosition.y() - m_objectifY == -20) && (m_snakePosition.x() - m_objectifX == 1))
        {
                m_objectifX = (rand() % 24) * 20;
                m_objectifY = (rand() % 24) * 20;
            goto recalculer;
        }
     break;
    }

    // on verifie si on tombe pas sur la tail du snake
    for (int i(1);i<m_snakeImages.size();i++)
    {
        if ((m_snakeImages[i]->x() - m_objectifX <= 2 && m_snakeImages[i]->x() - m_objectifX >= 0) && (m_snakeImages[i]->y() - m_objectifY <= 2 && m_snakeImages[i]->y() - m_objectifY >= 0) )
        {
            m_objectifX = (rand() % 24) * 20;
            m_objectifY = (rand() % 24) * 20;
            goto recalculer;
        }
    }

    m_objectif->setPos(m_objectifX,m_objectifY);//on change la position de l'obj apres le calcul de la precision

    if (m_pause)//si on etait dans une pause on relance le comptage normal
    {
        m_pause = false;
    }

}

void SnakeSimpleGame::snakeTranslation()
{
    switch (m_snakeDirections.at(0))//on effectue une translation selon la direction de la tete du snake et si on rencontre l'objectif on change la position de l'obj
    {
        case m_right:
         if (m_snakePosition.x() == 480)
             m_snakePosition.setX(-20);
         else
          m_snakePosition.setX(m_snakePosition.x()+20);
           eatingObjectif();
          break;
        case m_left:
        if (m_snakePosition.x() == -10)
            m_snakePosition.setX(490);
        else
          m_snakePosition.setX(m_snakePosition.x()-20);
            eatingObjectif();
          break;
        case m_up:
        if (m_snakePosition.y() == -10)
            m_snakePosition.setY(490);
        else
          m_snakePosition.setY(m_snakePosition.y()-20);
            eatingObjectif();
          break;
        case m_down:
        if (m_snakePosition.y() == 480)
            m_snakePosition.setY(-20);
        else
          m_snakePosition.setY(m_snakePosition.y()+20);
            eatingObjectif();
        break;
    }

    snakeTailTranslation();
    m_snakeImages.at(0)->setPos(m_snakePosition);
    gameOver();
}







