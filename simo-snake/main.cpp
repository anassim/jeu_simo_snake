#include <QApplication>
#include <QtGui>
#include <QMainWindow>
#include <cstdlib>
#include <ctime>
#include "snake.h"
#include "menusnake.h"
#include "snakesimplegame.h"
#include "snakemultjoueurs.h"
#include "snakemapgame.h"
#include "snakescores.h"

int main(int argc,char *argv[])
{
    /* ON LANCE LE JEU SIMO_SNAKE*/

    QApplication app(argc,argv);

    srand(static_cast<unsigned int>(time(nullptr)));//pour inisialiser le nombre aleatoire

    Snake jeu;

    jeu.show();

    return app.exec();
}
