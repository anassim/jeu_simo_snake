#ifndef SNAKEMULTJOUEURS_H
#define SNAKEMULTJOUEURS_H

#include <QApplication>
#include <QObject>
#include <QMainWindow>
#include <QLabel>
#include <QEvent>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPointF>
#include <QMediaPlayer>
#include <QSettings>
#include <QDialog>
#include <QTcpServer>
#include <QTcpSocket>
#include <QPushButton>
#include <QLineEdit>
#include <QSpinBox>
#include <QValidator>
#include <QTimeEdit>
#include <QTime>

class Connexion :public QDialog //LA BOIRE POUR SE CONNECTER AU SERVEURS OU LANCER UN SERVEUR
{
    Q_OBJECT

         public:
        Connexion()
        {
            setFixedSize(250,250);
            QLabel *m_background = new QLabel(this);
            m_background->setPixmap(QPixmap("imgs/Multijoueurs/mini background.png"));
            m_background->setGeometry(0,0,250,250);
            m_ip = new QLineEdit(this);
            m_ip->setGeometry(30,20,90,25);
            m_ip->setText("127.0.0.1");
            QRegExp rx("[0-9]{1,3}\\.{1,1}[0-9]{1,3}\\.{1,1}[0-9]{1,3}\\.{1,1}[0-9]{1,3}");
            QValidator *validateur = new QRegExpValidator(rx,this);
            m_ip->setValidator(validateur);
            QPalette palette1;palette1.setColor(QPalette::Text,QColor(152,19,18));
            QPalette palette2;palette2.setBrush(QPalette::Active,QPalette::WindowText,QBrush(QColor(17,63,19)));
            m_ip->setPalette(palette1);
            m_port = new QSpinBox(this);
            m_port->setGeometry(130,20,90,25);
            m_port->setRange(5000,65000);
            m_port->setValue(55000);
            rx.setPattern("[0-9]{1,2}");
            QValidator *pat = new QRegExpValidator(rx,this);
            m_minute = new QLineEdit(this);
            m_minute->setGeometry(80,60,40,25);
            m_minute->setValidator(pat);
            m_minute->setText("00");
            m_seconde = new QLineEdit(this);
            m_seconde->setGeometry(130,60,40,25);
            m_seconde->setValidator(pat);
            m_seconde->setText("00");
            m_connexion = new QPushButton("connexion",this);
            m_connexion->setGeometry(30,120,90,25);
            m_server = new QPushButton("lancer serveur",this);
            m_server->setGeometry(130,120,90,25);
            m_deconnexion = new QPushButton("deconnexion",this);
            m_deconnexion->setGeometry(80,155,90,25);
            m_etatConnexion = new QLabel(this);
            m_etatConnexion->setGeometry(20,160,250,100);
            m_etatConnexion->setPalette(palette2);
            m_etatConnexion->setText("Veillez lancer un serveur<br/>ou vou connecter a un serveur");

            QObject::connect(m_connexion,SIGNAL(clicked()),this,SLOT(Connecter()));
            QObject::connect(m_server,SIGNAL(clicked()),this,SLOT(lancerUnServeur()));
            QObject::connect(m_deconnexion,SIGNAL(clicked()),this,SLOT(Deconnecter()));
        }
        Connexion(QString Ip,quint16 port,int timer)
        {
            setFixedSize(250,250);
            QLabel *m_background = new QLabel(this);
            m_background->setPixmap(QPixmap("imgs/Multijoueurs/mini background.png"));
            m_background->setGeometry(0,0,250,250);
            m_ip = new QLineEdit(this);
            m_ip->setGeometry(30,20,90,25);
            m_ip->setText(Ip);
            QRegExp rx("[0-9]{1,3}\\.{1,1}[0-9]{1,3}\\.{1,1}[0-9]{1,3}\\.{1,1}[0-9]{1,3}");
            QValidator *validateur = new QRegExpValidator(rx,this);
            m_ip->setValidator(validateur);
            QPalette palette1;palette1.setColor(QPalette::Text,QColor(152,19,18));
            QPalette palette2;palette2.setBrush(QPalette::Active,QPalette::WindowText,QBrush(QColor(17,63,19)));
            m_ip->setPalette(palette1);
            m_port = new QSpinBox(this);
            m_port->setGeometry(130,20,90,25);
            m_port->setRange(5000,65000);
            m_port->setValue(port);
            rx.setPattern("[0-9]{1,2}");
            QValidator *pat = new QRegExpValidator(rx,this);
            m_minute = new QLineEdit(this);
            m_minute->setGeometry(80,60,40,25);
            m_minute->setValidator(pat);
            QString min;min.setNum(timer/1000/60);
            m_minute->setText(min);
            m_seconde = new QLineEdit(this);
            m_seconde->setGeometry(130,60,40,25);
            m_seconde->setValidator(pat);
            QString sec;sec.setNum(timer/1000-min.toInt()*60);
            m_seconde->setText(sec);
            m_connexion = new QPushButton("connexion",this);
            m_connexion->setGeometry(30,120,90,25);
            m_server = new QPushButton("lancer serveur",this);
            m_server->setGeometry(130,120,90,25);
            m_deconnexion = new QPushButton("deconnexion",this);
            m_deconnexion->setGeometry(80,155,90,25);
            m_deconnexion->setEnabled(false);
            m_etatConnexion = new QLabel(this);
            m_etatConnexion->setGeometry(20,160,250,100);
            m_etatConnexion->setPalette(palette2);
            m_etatConnexion->setText("VOULEZ VOUS REJOUER LA PARTIE !!<br/> tout est pret.<br/>cliquez sur un des boutons");

            QObject::connect(m_connexion,SIGNAL(clicked()),this,SLOT(Connecter()));
            QObject::connect(m_server,SIGNAL(clicked()),this,SLOT(lancerUnServeur()));
            QObject::connect(m_deconnexion,SIGNAL(clicked()),this,SLOT(Deconnecter()));
        }

        QString getIp() const
        {
            return m_ip->text();
        }
        quint16 getPort() const
        {
            return  static_cast<quint16>(m_port->value());
        }
        int getTime() const
        {
            return (m_minute->text().toInt()*60 + m_seconde->text().toInt())*1000;
        }
        void setEtat(QString etat)
        {
            m_etatConnexion->setText(etat);
        }

        ~Connexion()
        {

        }

         protected slots:
        void Connecter()//pour se connecter a un serveur deja lance
        {
            emit seConnecter();
            m_connexion->setEnabled(false);
            m_server->setEnabled(false);
            m_deconnexion->setEnabled(true);
            m_deconnexion->setFocus();
        }
        void lancerUnServeur()//lancer un serveur pour q'un autre client se connecte
        {
            emit lancerServeur();
            m_connexion->setEnabled(false);
            m_server->setEnabled(false);
            m_deconnexion->setEnabled(true);
        }
        void Deconnecter()
        {
            emit seDeconnecter();//pour se deconnecter si le serveur est lance ou le client se connecte
            m_connexion->setEnabled(true);
            m_server->setEnabled(true);
        }

         signals:
        void seConnecter();//on l'emit pour se connecter a un serveur
        void lancerServeur();//on l'emit pour lancer un serveur
        void seDeconnecter();//on l'emit pour se deconnecter de notre liason

         protected:
        QLineEdit *m_ip;
        QSpinBox *m_port;
        QLineEdit *m_minute;
        QLineEdit *m_seconde;
        QPushButton *m_connexion;
        QPushButton *m_server;
        QPushButton *m_deconnexion;
        QLabel *m_etatConnexion;
};

class SnakeMultijoueurs :public QGraphicsView // c'est une view de la scene pour le multijoueurs
{
     Q_OBJECT

     public:
     SnakeMultijoueurs();
     SnakeMultijoueurs(QString Ip,quint16 port,int timing);
     void chargerOptions();//pour charger les options sauvgardes dans le fichier des options et scores
     int key_num(QString key)const;//pour charger le nombre du bouton
     void snakeTailTranslation();//pour la translation de la queu du Snake
     void ajouterTail();//c'est pour ajouter une queu en mangant un objectif
     void eatingObjectif();//pour verifier si on mange un objectif
     void gameOver();//apres avoir perdu
     void theEndResult(int result);//pour envoyer le resultat du jeu a l'adversaire

     ~SnakeMultijoueurs();

     protected slots:
     void connecter();//pour se connecter
     void lancerUnServeur();//pour lancer un seveur
     void deconnecter();//pour se deconnecter
     void nouvelleConnexion();//si on a lance un serveur et un client vient de se connecte
     void donneesRecues();//apres qu'on s'est connecte on recoit des donnes
     void deconnexionClient();//on gere la deconnexion du client
     void connecte();//on se connectant a un serveur
     void deconnecte();//on se deconnectant du serveur
     void erreurSocket(QAbstractSocket::SocketError);
     void rejouerSon(qint64 position);//pour rejouer le son a sa fin
     void keyPressEvent(QKeyEvent *event);//pour manipuler les clics du clavier
     void chargerObjectifPosition();//charger la position de l'objectif
     void snakeTranslation();//changer de position selon la direction a la fin du timer
     void envoyerSnake();//pour envoyer notre position a l'adv
     void gameEnd();//pour alerter la fin du jeu

     signals:
     void snakeMultijoueursOut();//on l'emit pour sortir du multijoueurs au menu
     void newMultijoueurs(QString Ip,quint16 port,int timer);//on l'emit pour rejouer la partie

     protected:
     bool rejouer;
     Connexion *m_connecter;//la boite de connexion et le client et le serveur
     QTcpServer *m_serveur;
     QTcpSocket *m_client;
     QString Ip;
     quint16 port;
     int timer;
     QTimer *m_gameTime;//lancer le timer si on est client
     quint16 m_tailleMsg;//la taille du msg envoye
     quint8 m_typeMsg;//le type du msg envoye
     QVector<int> m_options;//les options utilise pour le jeu
     QMediaPlayer *m_sonMultijoueurs;//pour le son du jeu
     QGraphicsScene *m_scene;//pour la scene du jeu
     QGraphicsPixmapItem *m_objectif;//pour l'image de l'objectif et sa position le temps de changement de pos
     int m_objectifX,m_objectifY;//la position de l'objectif
     QTimer *m_chargerPositionObjectifTime;//lancer le timer de l'objectif si on est serveur
     QVector<QGraphicsPixmapItem*> m_snakeImages;//pour l'image et la position et la direction et le temp de changement du snake
     QVector<int> m_snakeDirections;//
     QTimer *m_envoieSnakeTime;//pour le temps du chargement de la position du snake adverse
     QVector<QGraphicsPixmapItem*> m_snakeAdvImages;//pour l'image et la direction et le temp de changement du snake adverse
     QPoint m_snakePosition;//la position de la tete du snake
     QPoint m_snakeAdvPosition;//la position de la tete du snake adverse
     QTimer *m_chargerPositionSnakeTime;
     bool m_pause;//pour la pause et la fin
     QGraphicsPixmapItem *m_pauseImage;//l'image de pause affiche
     QPixmap imagePause;//copier l'image de pause pour la modifier
     QGraphicsTextItem *m_afficherScore;
     bool m_pauseOut;//une variable de pause qui n'a aucun effet sur le jeu "on peut la suprimmer"
     QMediaPlayer *m_eatingObjSound;//pour le son apres manger un objectif
     QMediaPlayer *m_sonGameOver;//pour le son de Game Over
     QMediaPlayer *m_sonGoodGame;//pour le son de Good Game
     QSettings *m_scores_options;//pour enregistrer les scores et avoir les options
};

#endif // SNAKEMULTJOUEURS_H
