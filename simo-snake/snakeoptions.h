#ifndef SNAKEOPTIONS_H
#define SNAKEOPTIONS_H

#include <QApplication>
#include <QObject>
#include <QMainWindow>
#include <QLabel>
#include <QEvent>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPointF>
#include <QMediaPlayer>
#include <QSettings>
#include <QTextEdit>

class SnakeOptions :public QWidget
{
     Q_OBJECT

     public:
    SnakeOptions(int indexHautParleur,qint64 sonPosition);
    int getIndexHautParleur()const;//retourner l'index du haut parleur
    qint64 getSonPosition()const;//retourner la position du son
    void afficherOptions();//pour afficher les options sur l'image
    void alternerOptions(QString touche,int option);//si la touche choisie est trouve dans une autre option on l'efface dans cella

    ~SnakeOptions();

     protected slots:
     void rejouerSon(qint64 position);//pour rejouer le son a sa fin
     void mousePressEvent(QMouseEvent *event);//pour manipuler les clics de souris
     void keyPressEvent(QKeyEvent *event);//pour manipuler les clics du clavier

     signals:
     void snakeOptionsOut();//on l'emit pour sortir du menu Options au menu principal


     protected:
    QSettings *m_Options;//pour ouvrir le fichier Options
    QLabel *m_imageOptions;//pour l'image du menu Options
    int m_indexHautParleur;//pour le mode et images du haut parleur
    QLabel *m_hautParleur;
    QMediaPlayer *m_sonOptions;//pour ajouter du son
    int m_optionChoisi;
    /*c'est les zone d'options*/
    QLabel *m_up;
    QLabel *m_down;
    QLabel *m_right;
    QLabel *m_left;
    QLabel *m_pause;
    QLabel *m_rejouer;
    QLabel *m_revenir;
    QLabel *m_sortir;
    QLabel *m_son;
};
#endif // SNAKEOPTIONS_H
