#ifndef SNAKESCORES_H
#define SNAKESCORES_H

#include <QApplication>
#include <QObject>
#include <QMainWindow>
#include <QLabel>
#include <QEvent>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPointF>
#include <QMediaPlayer>
#include <QSettings>

class SnakeScores :public QWidget
{
     Q_OBJECT

     public:
    SnakeScores(int indexHautParleur,qint64 sonPosition);
    int getIndexHautParleur()const;//retourner l'index du haut parleur
    qint64 getSonPosition()const;//retourner la position du son
    void afficherScores()const;//pour afficher le scores sur l'image du SnakeScores en les dessinant

    ~SnakeScores();

     protected slots:
     void rejouerSon(qint64 position);//pour rejouer le son a sa fin
     void mousePressEvent(QMouseEvent *event);//pour manipuler les clics de souris
     void keyPressEvent(QKeyEvent *event);//pour manipuler les clics du clavier

     signals:
     void snakeScoresOut();//on l'emit pour sortir du menu scores au menu principal


     protected:
    QSettings *m_scores;//pour ouvrir le fichier scores
    QStringList m_listeScores;
    QLabel *m_imageScores;//pour l'image du menu scores
    int m_indexHautParleur;//pour le mode et images du haut parleur
    QLabel *m_hautParleur;
    QMediaPlayer *m_sonScores;//pour ajouter du son
};

#endif // SNAKESCORES_H
