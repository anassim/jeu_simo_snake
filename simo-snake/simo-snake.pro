QT += widgets multimedia

HEADERS += \
    menusnake.h \
    snake.h \
    snakemapgame.h \
    snakemultjoueurs.h \
    snakeoptions.h \
    snakescores.h \
    snakesimplegame.h

SOURCES += \
    main.cpp \
    menusnake.cpp \
    snake.cpp \
    snakemapgame.cpp \
    snakemultjoueurs.cpp \
    snakeoptions.cpp \
    snakescores.cpp \
    snakesimplegame.cpp
