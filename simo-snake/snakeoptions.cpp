#include <QtGui>
#include <QSettings>
#include <QPainter>
#include <QEvent>
#include <QPalette>
#include "snakeOptions.h"

//=============================================================================================================================
                            // LES METHODES DU SnakeOptions

SnakeOptions::SnakeOptions(int indexHautParleur,qint64 sonPosition)
{
    m_imageOptions = new QLabel(this);
    m_imageOptions->setPixmap(QPixmap("imgs/MenuOptions/Options.png"));
    m_Options = new QSettings("scores_options/scores_options.ini",QSettings::IniFormat,this);
    m_Options->beginGroup("options");
    afficherOptions();
    m_Options->endGroup();

    m_indexHautParleur = indexHautParleur;
    m_hautParleur = new QLabel(this);
    if (m_indexHautParleur == 1)
        m_hautParleur->setPixmap(QPixmap("imgs/menuImage/haut parleur/son lance.png"));
    else
        m_hautParleur->setPixmap(QPixmap("imgs/menuImage/haut parleur/son coupe.png"));
    m_hautParleur->setGeometry(445,10,45,45);

    m_sonOptions = new QMediaPlayer(this);
    m_sonOptions->setMedia(QUrl("sons/son menu.mp3"));
    m_sonOptions->setPosition(sonPosition + 40);//pour pousser unpeu la video en entrant
    m_sonOptions->play();
    if (m_indexHautParleur == 1)
        m_sonOptions->setMuted(false);
    else
        m_sonOptions->setMuted(true);

    m_optionChoisi = 0;

    grabKeyboard();

    QObject::connect(m_sonOptions,SIGNAL(positionChanged(qint64)),this,SLOT(rejouerSon(qint64)));
}

int SnakeOptions::getIndexHautParleur()const
{
    return m_indexHautParleur;
}

qint64 SnakeOptions::getSonPosition()const
{
    return m_sonOptions->position();
}

void SnakeOptions::afficherOptions()
{
    QPalette Palette(QColor::fromRgb(83,83,83));
    QFont Font("Impact",15);
    m_up = new QLabel(this);
    m_up->setGeometry(328,100,45,26);
    m_up->setPalette(Palette);
    m_up->setFont(Font);
    m_up->setText(m_Options->value("set_up").toString());
    m_down = new QLabel(this);
    m_down->setGeometry(328,135,45,26);
    m_down->setPalette(Palette);
    m_down->setFont(Font);
    m_down->setText(m_Options->value("set_down").toString());
    m_right = new QLabel(this);
    m_right->setGeometry(328,170,45,26);
    m_right->setPalette(Palette);
    m_right->setFont(Font);
    m_right->setText(m_Options->value("set_right").toString());
    m_left = new QLabel(this);
    m_left->setGeometry(328,202,45,26);
    m_left->setPalette(Palette);
    m_left->setFont(Font);
    m_left->setText(m_Options->value("set_left").toString());
    m_pause = new QLabel(this);
    m_pause->setGeometry(328,237,45,26);
    m_pause->setPalette(Palette);
    m_pause->setFont(Font);
    m_pause->setText(m_Options->value("pause").toString());
    m_rejouer = new QLabel(this);
    m_rejouer->setGeometry(328,270,45,26);
    m_rejouer->setPalette(Palette);
    m_rejouer->setFont(Font);
    m_rejouer->setText(m_Options->value("rejouer").toString());
    m_revenir = new QLabel(this);
    m_revenir->setGeometry(328,304,45,26);
    m_revenir->setPalette(Palette);
    m_revenir->setFont(Font);
    m_revenir->setText(m_Options->value("revenir_au_menu").toString());
    m_sortir = new QLabel(this);
    m_sortir->setGeometry(328,338,45,26);
    m_sortir->setPalette(Palette);
    m_sortir->setFont(Font);
    m_sortir->setText(m_Options->value("sortir_du_jeu").toString());
    m_son = new QLabel(this);
    m_son->setGeometry(328,374,45,26);
    m_son->setPalette(Palette);
    m_son->setFont(Font);
    m_son->setText(m_Options->value("son").toString());
}

void SnakeOptions::alternerOptions(QString touche,int option)
{
    if (m_up->text() == touche && option != 1)
        m_up->clear();
    else if (m_down->text() == touche && option != 2)
        m_down->clear();
    else if (m_right->text() == touche && option != 3)
        m_right->clear();
    else if (m_left->text() == touche && option != 4)
        m_left->clear();
    else if (m_pause->text() == touche && option != 5)
        m_pause->clear();
    else if (m_rejouer->text() == touche && option != 6)
        m_rejouer->clear();
    else if (m_revenir->text() == touche && option != 7)
        m_revenir->clear();
    else if (m_sortir->text() == touche && option != 8)
        m_sortir->clear();
    else if (m_son->text() == touche && option != 9)
        m_son->clear();
}

SnakeOptions::~SnakeOptions()
{
    m_Options->beginGroup("options");

    m_Options->setValue("set_up",m_up->text());
    m_Options->setValue("set_down",m_down->text());
    m_Options->setValue("set_right",m_right->text());
    m_Options->setValue("set_left",m_left->text());
    m_Options->setValue("pause",m_pause->text());
    m_Options->setValue("rejouer",m_rejouer->text());
    m_Options->setValue("revenir_au_menu",m_revenir->text());
    m_Options->setValue("sortir_du_jeu",m_sortir->text());
    m_Options->setValue("son",m_son->text());

    m_Options->endGroup();
}

//=============================================================================================================================
                            // LES SLOTS DU SnakeOptions

void SnakeOptions::rejouerSon(qint64 position)
{
    if (position >= 35000)//on rejoue le son apres chaque 35 secondes
        m_sonOptions->setPosition(1800);
}

void SnakeOptions::mousePressEvent(QMouseEvent *event)
{
                /* SI LE BOUTON GAUCHE EST CLIQUE*/

    if (event->button() == Qt::LeftButton)
    {
        if ((event->x() > 445 && event->x() < 490) && (event->y() > 10 && event->y() < 55))// donc on lance ou on coupe le son en changant l'image
        {
            if (m_indexHautParleur == 1)//le volume est lance donc on le stop
            {
                m_sonOptions->setMuted(true);
                m_hautParleur->setPixmap(QPixmap("imgs/menuImage/haut parleur/son coupe.png"));
                m_indexHautParleur = 2;
            }
            else if (m_indexHautParleur == 2)//le volume est arrete donc on le lance
            {
                m_sonOptions->setMuted(false);
                m_hautParleur->setPixmap(QPixmap("imgs/menuImage/haut parleur/son lance.png"));
                m_indexHautParleur = 1;
            }

         }
        else if ((event->x() > 32 && event->x() < 86) && (event->y() > 458 && event->y() < 487))//en cliquant sur la fleche de retour au menu
        {
            emit snakeOptionsOut();
        }


        else if ((event->x() > 320 && event->x() < 384) && (event->y() > 100 && event->y() < 126))
        {
            m_optionChoisi = 1;
        }
        else if ((event->x() > 320 && event->x() < 384) && (event->y() > 135 && event->y() < 161))
        {
            m_optionChoisi = 2;
        }
        else if ((event->x() > 320 && event->x() < 384) && (event->y() > 170 && event->y() < 196))
        {
            m_optionChoisi = 3;
        }
        else if ((event->x() > 320 && event->x() < 384) && (event->y() > 202 && event->y() < 228))
        {
            m_optionChoisi = 4;
        }
        else if ((event->x() > 320 && event->x() < 384) && (event->y() > 237 && event->y() < 263))
        {
            m_optionChoisi = 5;
        }
        else if ((event->x() > 320 && event->x() < 384) && (event->y() > 270 && event->y() < 296))
        {
            m_optionChoisi = 6;
        }
        else if ((event->x() > 320 && event->x() < 384) && (event->y() > 304 && event->y() < 330))
        {
            m_optionChoisi = 7;
        }
        else if ((event->x() > 320 && event->x() < 384) && (event->y() > 338 && event->y() < 364))
        {
            m_optionChoisi = 8;
        }
        else if ((event->x() > 320 && event->x() < 384) && (event->y() > 374 && event->y() < 400))
        {
            m_optionChoisi = 9;
        }
        else
        {
            m_optionChoisi = 0;
        }
    }
}

void SnakeOptions::keyPressEvent(QKeyEvent *event)
{
      if (event->key() == Qt::Key_Backspace)
      {
          emit snakeOptionsOut();
          return;
      }
      else if (event->key() == Qt::Key_Tab)
      {
          m_optionChoisi += 1;
          if (m_optionChoisi == 10)
              m_optionChoisi = 1;
          return;
      }
      if (m_optionChoisi > 0)
      {
          switch (m_optionChoisi)
          {
          case 1:
            switch (event->key())
            {
            case Qt::Key_Up:
                m_up->setText("Up");
                break;
            case Qt::Key_Down:
                m_up->setText("Down");
                break;
            case Qt::Key_Right:
                m_up->setText("Right");
                break;
            case Qt::Key_Left:
                m_up->setText("Left");
                break;
            default:
                m_up->setText(event->text().toUpper());
                break;
            }
            alternerOptions(m_up->text(),1);
            break;
          case 2:
            switch (event->key())
            {
            case Qt::Key_Up:
                m_down->setText("Up");
                break;
            case Qt::Key_Down:
                m_down->setText("Down");
                break;
            case Qt::Key_Right:
                m_down->setText("Right");
                break;
            case Qt::Key_Left:
                m_down->setText("Left");
                break;
            default:
                m_down->setText(event->text().toUpper());
                break;
            }
            alternerOptions(m_down->text(),2);
            break;
          case 3:
            switch (event->key())
            {
            case Qt::Key_Up:
                m_right->setText("Up");
                break;
            case Qt::Key_Down:
                m_right->setText("Down");
                break;
            case Qt::Key_Right:
                m_right->setText("Right");
                break;
            case Qt::Key_Left:
                m_right->setText("Left");
                break;
            default:
                m_right->setText(event->text().toUpper());
                break;
            }
            alternerOptions(m_right->text(),3);
            break;
          case 4:
            switch (event->key())
            {
            case Qt::Key_Up:
                m_left->setText("Up");
                break;
            case Qt::Key_Down:
                m_left->setText("Down");
                break;
            case Qt::Key_Right:
                m_left->setText("Right");
                break;
            case Qt::Key_Left:
                m_left->setText("Left");
                break;
            default:
                m_left->setText(event->text().toUpper());
                break;
            }
            alternerOptions(m_left->text(),4);
            break;
          case 5:
            switch (event->key())
            {
            case Qt::Key_Up:
                m_pause->setText("Up");
                break;
            case Qt::Key_Down:
                m_pause->setText("Down");
                break;
            case Qt::Key_Right:
                m_pause->setText("Right");
                break;
            case Qt::Key_Left:
                m_pause->setText("Left");
                break;
            default:
                m_pause->setText(event->text().toUpper());
                break;
            }
            alternerOptions(m_pause->text(),5);
            break;
          case 6:
            switch (event->key())
            {
            case Qt::Key_Up:
                m_rejouer->setText("Up");
                break;
            case Qt::Key_Down:
                m_rejouer->setText("Down");
                break;
            case Qt::Key_Right:
                m_rejouer->setText("Right");
                break;
            case Qt::Key_Left:
                m_rejouer->setText("Left");
                break;
            default:
                m_rejouer->setText(event->text().toUpper());
                break;
            }
            alternerOptions(m_rejouer->text(),6);
            break;
          case 7:
            switch (event->key())
            {
            case Qt::Key_Up:
                m_revenir->setText("Up");
                break;
            case Qt::Key_Down:
                m_revenir->setText("Down");
                break;
            case Qt::Key_Right:
                m_revenir->setText("Right");
                break;
            case Qt::Key_Left:
                m_revenir->setText("Left");
                break;
            default:
                m_revenir->setText(event->text().toUpper());
                break;
            }
            alternerOptions(m_revenir->text(),7);
            break;
          case 8:
            switch (event->key())
            {
            case Qt::Key_Up:
                m_sortir->setText("Up");
                break;
            case Qt::Key_Down:
                m_sortir->setText("Down");
                break;
            case Qt::Key_Right:
                m_sortir->setText("Right");
                break;
            case Qt::Key_Left:
                m_sortir->setText("Left");
                break;
            default:
                m_sortir->setText(event->text().toUpper());
                break;
            }
            alternerOptions(m_sortir->text(),8);
            break;
          case 9:
            switch (event->key())
            {
            case Qt::Key_Up:
                m_son->setText("Up");
                break;
            case Qt::Key_Down:
                m_son->setText("Down");
                break;
            case Qt::Key_Right:
                m_son->setText("Right");
                break;
            case Qt::Key_Left:
                m_son->setText("Left");
                break;
            default:
                m_son->setText(event->text().toUpper());
                break;
            }
            alternerOptions(m_son->text(),9);
            break;
          }
      }
}


