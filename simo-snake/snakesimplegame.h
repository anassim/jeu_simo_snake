#ifndef SNAKESIMPLEGAME_H
#define SNAKESIMPLEGAME_H

#include <QApplication>
#include <QObject>
#include <QMainWindow>
#include <QLabel>
#include <QEvent>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPointF>
#include <QMediaPlayer>
#include <QSettings>


class SnakeSimpleGame :public QGraphicsView // c'est une view de la scene pour le simple jeu
{
     Q_OBJECT

     public:
     SnakeSimpleGame();
     void chargerOptions();//pour charger les options sauvgardes dans le fichier des options et scores
     int key_num(QString key)const;//pour charger le nombre du bouton
     void chargerSnakePosition();//charger la position du snake
     void snakeTailTranslation();//pour la translation de la queu du Snake
     void ajouterTail();//c'est pour ajouter une queu en mangant un objectif
     void eatingObjectif();//pour verifier si on mange un objectif
     void gameOver();//apres avoir perdu
     void sauvgarderScore();//pour sauvgarder le score

     ~SnakeSimpleGame();

     protected slots:
     void rejouerSon(qint64 position);//pour rejouer le son a sa fin
     void keyPressEvent(QKeyEvent *event);//pour manipuler les clics du clavier
     void chargerObjectifPosition();//charger la position de l'objectif
     void snakeTranslation();//changer de position selon la direction a la fin du timer

     signals:
     void snakeSimpleGameOut();//on l'emit pour sortir du simple jeu au menu
     void newSimpleGame();//on l'emit pour rejouer la partie

     protected:
     QVector<int> m_options;//tableau pour les options
     QMediaPlayer *m_sonSimpleGame;//pour le son du jeu
     QGraphicsScene *m_scene;//pour la scene du jeu
     QGraphicsPixmapItem *m_objectif;//pour l'image de l'objectif et sa position le temps de changement de pos
     int m_objectifX,m_objectifY;
     QTimer *m_chargerPositionObjectifTime;
     QVector<QGraphicsPixmapItem*> m_snakeImages;//pour l'image et la position et la direction et le temp de changement du snake
     QVector<int> m_snakeDirections;
     QPoint m_snakePosition;//la position de la tete du snake
     QTimer *m_chargerPositionSnakeTime;
     bool m_pause;//pour la pause
     QGraphicsPixmapItem *m_pauseImage;
     QPixmap imagePause;
     bool m_pauseOut;
     QMediaPlayer *m_eatingObjSound;//pour le son apres manger un objectif
     QMediaPlayer *m_sonGameOver;//pour le son de Game Over
     QMediaPlayer *m_sonGoodGame;//pour le son de Good Game
     QSettings *m_scores_options;//pour enregistrer les scores et avoir les options
     QStringList m_listeScores;
     int meilleureScore;
     QGraphicsTextItem *m_afficherScore;
};

#endif // SNAKESIMPLEGAME_H
