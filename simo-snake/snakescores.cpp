#include <QtGui>
#include <QSettings>
#include <QPainter>
#include <QEvent>
#include "snakescores.h"

//=============================================================================================================================
                            // LES METHODES DU SnakeScores

SnakeScores::SnakeScores(int indexHautParleur,qint64 sonPosition)
{
    m_imageScores = new QLabel(this);
    m_imageScores->setPixmap(QPixmap("imgs/MenuScores/scores.png"));
    m_scores = new QSettings("scores_options/scores_options.ini",QSettings::IniFormat,this);
    m_scores->beginGroup("scores");
    m_listeScores = m_scores->allKeys();
    afficherScores();
    m_scores->endGroup();

    m_indexHautParleur = indexHautParleur;
    m_hautParleur = new QLabel(this);
    if (m_indexHautParleur == 1)
        m_hautParleur->setPixmap(QPixmap("imgs/menuImage/haut parleur/son lance.png"));
    else
        m_hautParleur->setPixmap(QPixmap("imgs/menuImage/haut parleur/son coupe.png"));
    m_hautParleur->setGeometry(445,10,45,45);

    m_sonScores = new QMediaPlayer(this);
    m_sonScores->setMedia(QUrl("sons/son menu.mp3"));
    m_sonScores->setPosition(sonPosition + 40);//pour pousser unpeu la video en entrant
    m_sonScores->play();
    if (m_indexHautParleur == 1)
        m_sonScores->setMuted(false);
    else
        m_sonScores->setMuted(true);

    grabKeyboard();

    QObject::connect(m_sonScores,SIGNAL(positionChanged(qint64)),this,SLOT(rejouerSon(qint64)));
}

int SnakeScores::getIndexHautParleur()const
{
    return m_indexHautParleur;
}

qint64 SnakeScores::getSonPosition()const
{
    return m_sonScores->position();
}

void SnakeScores::afficherScores()const
{
    QPixmap background("imgs/MenuScores/scores.png");
    QPainter dessinerScores;
    dessinerScores.begin(&background);
    dessinerScores.setPen(QColor::fromRgb(117,32,29));
    dessinerScores.setFont(QFont("Chalkduster",20));
    dessinerScores.drawText(QPointF(323,149),m_scores->value(m_listeScores[0]).toString());
    dessinerScores.drawText(QPointF(323,213),m_scores->value(m_listeScores[1]).toString());
    dessinerScores.drawText(QPointF(323,277),m_scores->value(m_listeScores[2]).toString());
    dessinerScores.drawText(QPointF(323,339),m_scores->value(m_listeScores[3]).toString());
    dessinerScores.drawText(QPointF(323,403),m_scores->value(m_listeScores[4]).toString());
    dessinerScores.end();
    m_imageScores->setPixmap(background);
}

SnakeScores::~SnakeScores()
{

}

//=============================================================================================================================
                            // LES SLOTS DU SnakeScores

void SnakeScores::rejouerSon(qint64 position)
{
    if (position >= 35000)//on rejoue le son apres chaque 35 secondes
        m_sonScores->setPosition(1800);
}

void SnakeScores::mousePressEvent(QMouseEvent *event)
{
                /* SI LE BOUTON GAUCHE EST CLIQUE*/

    if (event->button() == Qt::LeftButton)
    {
        if ((event->x() > 445 && event->x() < 490) && (event->y() > 10 && event->y() < 55))// donc on lance ou on coupe le son en changant l'image
        {
            if (m_indexHautParleur == 1)//le volume est lance donc on le stop
            {
                m_sonScores->setMuted(true);
                m_hautParleur->setPixmap(QPixmap("imgs/menuImage/haut parleur/son coupe.png"));
                m_indexHautParleur = 2;
            }
            else if (m_indexHautParleur == 2)//le volume est arrete donc on le lance
            {
                m_sonScores->setMuted(false);
                m_hautParleur->setPixmap(QPixmap("imgs/menuImage/haut parleur/son lance.png"));
                m_indexHautParleur = 1;
            }

         }
        else if ((event->x() > 32 && event->x() < 86) && (event->y() > 458 && event->y() < 487))//en cliquant sur la fleche de retour au menu
        {
            emit snakeScoresOut();
        }
    }
}

void SnakeScores::keyPressEvent(QKeyEvent *event)
{
      if (event->key() == Qt::Key_Backspace)
      {
          emit snakeScoresOut();
      }
}
