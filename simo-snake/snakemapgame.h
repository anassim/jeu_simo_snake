#ifndef SNAKEMAPGAME_H
#define SNAKEMAPGAME_H

#include <QApplication>
#include <QObject>
#include <QMainWindow>
#include <QLabel>
#include <QEvent>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPointF>
#include <QMediaPlayer>
#include <QSettings>
#include <QDialog>
#include <QComboBox>

class SnakeMapGame :public QGraphicsView // c'est une view de la scene pour le map game
{
     Q_OBJECT

     public:
     SnakeMapGame(QString mapName);
     void chargerOptions();//pour charger les options sauvgardes dans le fichier des options et scores
     int key_num(QString key)const;//pour charger le nombre du bouton
     void chargerMap();
     void reglageObjectsCollapsing();//suprimmer les objets qui ne peuvent pas etre touche par le snake
     void chargerSnakePosition();//charger la position du snake
     void snakeTailTranslation();//pour la translation de la queu du Snake
     void ajouterTail();//c'est pour ajouter une queu en mangant un objectif
     void eatingObjectif();//pour verifier si on mange un objectif
     void gameOver();//apres avoir perdu
     bool Collapsing(int direction,QString score);//verifier si on touche pas un des objects

     ~SnakeMapGame();

     protected slots:
     void rejouerSon(qint64 position);//pour rejouer le son a sa fin
     void keyPressEvent(QKeyEvent *event);//pour manipuler les clics du clavier
     void chargerObjectifPosition();//charger la position de l'objectif
     void snakeTranslation();//changer de position selon la direction a la fin du timer

     signals:
     void snakeMapGameOut();//on l'emit pour sortir du map game au menu
     void newMapGame(QString mapName);//on l'emit pour rejouer la partie

     protected:
     QVector<int> m_options;
     QMediaPlayer *m_sonMapGame;//pour le son du jeu
     QGraphicsScene *m_scene;//pour la scene du jeu
     QString m_mapName;//le nom de la map a jouer
     QStringList m_listeMaps;//la liste des maps
     QSettings *m_objects;//pour les positions des objets
     QStringList m_nombreObjects;//combien ya d'objects
     QStringList m_positionObjects;//pour avoir positions d'objects et non pas la position en nombre
     QVector<QGraphicsPixmapItem*> m_objectsImages;//pour les images des objets
     QVector<QGraphicsPixmapItem*> m_objectsCollapsing;//pour les objects qui peuvent etre touche par le snake
     QGraphicsPixmapItem *m_objectif;//pour l'image de l'objectif et sa position le temps de changement de pos
     int m_objectifX,m_objectifY;
     QTimer *m_chargerPositionObjectifTime;
     QVector<QGraphicsPixmapItem*> m_snakeImages;//pour l'image et la position et la direction et le temps de changement du snake
     QVector<int> m_snakeDirections;
     QPoint m_snakePosition;//la position de la tete du snake
     QTimer *m_chargerPositionSnakeTime;
     bool m_pause;//pour la pause
     QGraphicsPixmapItem *m_pauseImage;
     QPixmap imagePause;
     bool m_pauseOut;
     bool m_gameOver;
     QMediaPlayer *m_eatingObjSound;//pour le son apres manger un objectif
     QMediaPlayer *m_sonGameOver;//pour le son de Game Over
     QMediaPlayer *m_sonGoodGame;
     QSettings *m_scores_options;//pour enregistrer les scores et avoir les options
     int meilleureScore;
     QGraphicsTextItem *m_afficherScore;
};

#endif // SNAKEMAPGAME_H
