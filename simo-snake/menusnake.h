#ifndef MENUSNAKE_H
#define MENUSNAKE_H

#include <QApplication>
#include <QObject>
#include <QWidget>
#include <QMainWindow>
#include <QLabel>
#include <QMediaPlayer>
#include <QEvent>
#include <QDialog>
#include <QComboBox>
#include <QPushButton>

class Map :public QDialog //LA BOITE POUR CHOISIR LA MAP EN MAP GAME
{
    Q_OBJECT

         public:
        Map()
        {
            setFixedSize(300,300);
            m_mapImage = new QLabel(this);
            m_mapImage->setPixmap(QPixmap("imgs/MapGame/Table/mini background.png"));
            m_mapImage->setGeometry(0,0,300,300);
            m_listeMaps = new QComboBox(this);
            m_listeMaps->setGeometry(100,25,100,20);
            m_listeMaps->addItem("Table");
            m_listeMaps->addItem("Jardin");
            m_listeMaps->addItem("Prison");
            m_listeMaps->addItem("Hell");
            m_boutonChoisir = new QPushButton(this);
            m_boutonChoisir->setText("Jouer");
            m_boutonChoisir->setGeometry(115,265,70,30);
            m_boutonChoisir->setAutoFillBackground(true);
            m_boutonChoisir->setFlat(true);
            m_boutonChoisir->setPalette(QPalette(QColor::fromRgb(168,0,0)));

            QObject::connect(m_boutonChoisir,SIGNAL(clicked()),this,SLOT(jouer()));
            QObject::connect(m_listeMaps,SIGNAL(currentTextChanged(const QString)),this,SLOT(changerBackground(const QString)));
        }

        QString getMapName()//prendre le nom de la map
        {
            return m_listeMaps->currentText();
        }

        ~Map()
        {
           emit lancerMapGame();
        }

         protected slots:
        void jouer()//clique sur le bouton jouer
        {
            emit lancerMapGame();
        }
        void changerBackground(const QString currentMap)//changer le background en changeant la map
        {
            m_mapImage->setPixmap("imgs/MapGame/"+currentMap+"/mini background.png");
        }

         signals:
        void lancerMapGame();//on l'emit pour lancer le jeu


         protected:
        QComboBox *m_listeMaps;
        QLabel *m_mapImage;
        QPushButton *m_boutonChoisir;
};

class SnakeMenu :public QWidget //c'est le widget pour le menu de snake
{
     Q_OBJECT

     public:
    SnakeMenu();
    SnakeMenu(int indexHautParleur,qint64 sonPosition);
    int getIndexMenu()const;//retourner le num du menu
    int getIndexHautParleur()const;//retourner l'index du haut parleur
    qint64 getSonPosition()const;//retourner la position du son

    ~SnakeMenu();

     protected slots:
    void rejouerSon(qint64 position);//pour rejouer le son a sa fin
    void mousePressEvent(QMouseEvent *event);//pour manipuler les clics de souris
    void getMap();//prendre la map choisi

     signals:
    void menuSimpleGameClique();//on l'emit pour lancer un simple jeu
    void menuMapGameClique(QString mapName);//on l'emit pour lancer un map game
    void menuMultijoueursClique();//on l'emit pour entrer dans le mode multijoueurs
    void menuSnakeOptionsClique();//on l'emit pour lancer le menu options
    void menuSnakeScoresClique();//on l'emit pour lancer le menu scores

     protected:
    int m_indexMenu;//pour le mode et images du menu
    QLabel *m_menu;
    int m_indexHautParleur;//pour le mode et images du haut parleur
    QLabel *m_hautParleur;
    QMediaPlayer *m_sonMenu;//pour ajouter du son
    Map *m_choisirMap;//pour la boite de map
    QString m_mapName;//le nom de la map choisi
};

#endif // MENUSNAKE_H
