#include <QObject>
#include <QMainWindow>
#include <QLabel>
#include <QMediaPlayer>
#include <QEvent>
#include <QMouseEvent>
#include <QMessageBox>
#include <QComboBox>
#include "menusnake.h"

//=============================================================================================================================
                            // LES METHODES DU SnakeMenu

SnakeMenu::SnakeMenu()
{
    /* on inisialise le menu*/

    m_indexMenu = 1;
    m_menu = new QLabel(this);
    m_menu->setPixmap(QPixmap("imgs/MenuImage/menu snake.png"));
    m_menu->setGeometry(0,0,500,500);

    m_indexHautParleur = 1;
    m_hautParleur = new QLabel(this);
    m_hautParleur->setPixmap(QPixmap("imgs/menuImage/haut parleur/son lance.png"));
    m_hautParleur->setGeometry(445,10,45,45);

    m_sonMenu = new QMediaPlayer(this);
    m_sonMenu->setMedia(QUrl("sons/son menu.mp3"));
    m_sonMenu->play();

    m_choisirMap = new Map;

    QObject::connect(m_sonMenu,SIGNAL(positionChanged(qint64)),this,SLOT(rejouerSon(qint64)));
    QObject::connect(m_choisirMap,SIGNAL(lancerMapGame()),this,SLOT(getMap()));
}

SnakeMenu::SnakeMenu(int indexHautParleur,qint64 sonPosition)
{
    /* on inisialise le menu*/

    m_indexMenu = 1;
    m_menu = new QLabel(this);
    m_menu->setPixmap(QPixmap("imgs/MenuImage/menu snake.png"));
    m_menu->setGeometry(0,0,500,500);

    m_indexHautParleur = indexHautParleur;
    m_hautParleur = new QLabel(this);
    if (m_indexHautParleur == 1)
        m_hautParleur->setPixmap(QPixmap("imgs/menuImage/haut parleur/son lance.png"));
    else
        m_hautParleur->setPixmap(QPixmap("imgs/menuImage/haut parleur/son coupe.png"));
    m_hautParleur->setGeometry(445,10,45,45);

    m_sonMenu = new QMediaPlayer(this);
    m_sonMenu->setMedia(QUrl("sons/son menu.mp3"));
    m_sonMenu->setPosition(sonPosition + 40);//pour pousser unpeu la video en entrant
    m_sonMenu->play();
    if (m_indexHautParleur == 1)
        m_sonMenu->setMuted(false);
    else
        m_sonMenu->setMuted(true);

    m_choisirMap = new Map;

    QObject::connect(m_sonMenu,SIGNAL(positionChanged(qint64)),this,SLOT(rejouerSon(qint64)));
    QObject::connect(m_choisirMap,SIGNAL(lancerMapGame()),this,SLOT(getMap()));
}

int SnakeMenu::getIndexMenu()const
{
    return  m_indexMenu;
}

int SnakeMenu::getIndexHautParleur()const
{
    return m_indexHautParleur;
}

qint64 SnakeMenu::getSonPosition()const
{
    return m_sonMenu->position();
}

SnakeMenu::~SnakeMenu()
{
    m_choisirMap->deleteLater();
}

//=============================================================================================================================
                            // LES SLOTS DU SnakeMenu

void SnakeMenu::rejouerSon(qint64 position)
{
    if (position >= 35000)//on rejoue le son apres chaque 35 secondes
        m_sonMenu->setPosition(1800);
}

void SnakeMenu::mousePressEvent(QMouseEvent *event)
{
                /* SI LE BOUTON GAUCHE EST CLIQUE*/

    if (event->button() == Qt::LeftButton)
    {
        if ((event->x() > 445 && event->x() < 490) && (event->y() > 10 && event->y() < 55))// donc on lance ou on coupe le son en changant l'image
        {
            if (m_indexHautParleur == 1)//le volume est lance donc on le stop
            {
                m_sonMenu->setMuted(true);
                m_hautParleur->setPixmap(QPixmap("imgs/menuImage/haut parleur/son coupe.png"));
                m_indexHautParleur = 2;
            }
            else if (m_indexHautParleur == 2)//le volume est arrete donc on le lance
            {
                m_sonMenu->setMuted(false);
                m_hautParleur->setPixmap(QPixmap("imgs/menuImage/haut parleur/son lance.png"));
                m_indexHautParleur = 1;
            }

         }

       //-------------------------------------------------------------------

        if (m_indexMenu == 1)//si on est dans la premiere image du menu
        {
            if ((event->x() > 72 && event->x() < 285) && (event->y() > 141.7 && event->y() < 192.7))//donc on manipule les menus du simple game
            {
                 m_indexMenu = 2;
                 m_menu->setPixmap(QPixmap("imgs/MenuImage/menu snake simple game.png"));
                 m_menu->setGeometry(0,0,500,500);
            }
           else if ((event->x() > 72 && event->x() < 285) && (event->y() > 193 && event->y() < 244))
            {
                     m_choisirMap->exec();
                     int mapNum = rand() % 4;
                     if (m_mapName.isEmpty() && mapNum == 0)
                         m_mapName = "Table";
                     else if (m_mapName.isEmpty() && mapNum == 1)
                         m_mapName = "Jardin";
                     else if (m_mapName.isEmpty() && mapNum == 2)
                         m_mapName = "Prison";
                     else if (m_mapName.isEmpty() && mapNum == 3)
                         m_mapName = "Hell";
                     emit menuMapGameClique(m_mapName);
            }
            else if ((event->x() > 72 && event->x() < 285) && (event->y() > 244.7 && event->y() < 293.7))
             {
                      emit menuMultijoueursClique();
             }
           else if ((event->x() > 72 && event->x() < 285) && (event->y() > 294.7 && event->y() < 345.7))
            {
                     emit menuSnakeOptionsClique();
            }
           else if ((event->x() > 72 && event->x() < 285) && (event->y() > 345.7 && event->y() < 396.7))
            {
                    emit menuSnakeScoresClique();
            }
        }
         else if (m_indexMenu == 2)//si on est dans la deuxieme image du menu
         {
            if ((event->x() > 72 && event->x() < 285) && (event->y() > 141.7 && event->y() < 192.7))//ou donc on manipule les menus du simple game
            {
                    m_indexMenu = 1;
                    m_menu->setPixmap(QPixmap("imgs/MenuImage/menu snake.png"));
                    m_menu->setGeometry(0,0,500,500);
            }
            else if ((event->x() > 147.3 && event->x() < 273.2) && (event->y() > 193 && event->y() < 227))
              {
                 emit menuSimpleGameClique();//l'emission du signale pour entrer dans le simple jeu
              }
            else if ((event->x() > 72 && event->x() < 285) && (event->y() > 228 && event->y() < 274))
              {
                 m_choisirMap->exec();
                 int mapNum = rand() % 4;
                 if (m_mapName.isEmpty() && mapNum == 0)
                     m_mapName = "Table";
                 else if (m_mapName.isEmpty() && mapNum == 1)
                     m_mapName = "Jardin";
                 else if (m_mapName.isEmpty() && mapNum == 2)
                     m_mapName = "Prison";
                 else if (m_mapName.isEmpty() && mapNum == 3)
                     m_mapName = "Hell";
                 emit menuMapGameClique(m_mapName);
              }
            else if ((event->x() > 72 && event->x() < 285) && (event->y() > 274.7 && event->y() < 326))
             {
                      emit menuMultijoueursClique();
             }
            else if ((event->x() > 72 && event->x() < 252) && (event->y() > 327 && event->y() < 375))
              {
                emit menuSnakeOptionsClique();
              }
            else if ((event->x() > 72 && event->x() < 232) && (event->y() > 376 && event->y() < 424))
              {
                emit menuSnakeScoresClique();
              }
         }
        }
}

void SnakeMenu::getMap()
{
   m_mapName = m_choisirMap->getMapName();
   m_choisirMap->hide();
}


